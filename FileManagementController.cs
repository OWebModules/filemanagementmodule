﻿using CommandLineRunModule;
using ElasticSearchNestConnector;
using FileManagementModule.Models;
using FileManagementModule.Services;
using FileManagementModule.Validation;
using GenerateOntologyAssembly;
using GenerateOntologyAssembly.Models;
using ImportExport_Module;
using MediaStore_Module;
using MediaStore_Module.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Converters;
using OntoMsg_Module.Models;
using ReportModule;
using ReportModule.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule;
using TextParserModule.Models;
using TypedTaggingModule.Connectors;

namespace FileManagementModule
{
    public class FileManagementController : AppController
    {
        public async Task<ResultItem<RelateExternalFilesResult>> RelateExternalFile(RelateExternalFilesRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<RelateExternalFilesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelateExternalFilesResult()
                };

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Use has cancelled!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateReferenceExternalFilesRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ElasticAgent(Globals);

                var modelResult = await elasticAgent.GetRelateExternalFilesModel(request);

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Use has cancelled!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var fileWorkManager = new FileWorkManager(Globals);
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var folderObjects = modelResult.Result.ConfigToFolders.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Use has cancelled!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                var doSubFolders = modelResult.Result.SubItems.Val_Bool.Value;
                var createBlobFilesIfNew = modelResult.Result.CreateBlobIfNewFile?.Val_Bool ?? false;

                if (createBlobFilesIfNew)
                {
                    result.ResultState = await mediaStoreConnector.CheckMediaService();
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                request.MessageOutput?.OutputInfo("Check files...");

                foreach (var folderObject in folderObjects)
                {
                    var compareResult = await CompareFileItems(folderObject, 
                        fileWorkManager, 
                        mediaStoreConnector, 
                        elasticAgent, 
                        doSubFolders, 
                        createBlobFilesIfNew,
                        request.MessageOutput,
                        request.CancellationToken);

                    result.ResultState = compareResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    result.Result.FileItems.AddRange(compareResult.Result.FileItems);

                }

                request.MessageOutput?.OutputInfo("Checked files.");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RelateExternalFilesResult>> CompareFileItems(
            clsOntologyItem folderObject, 
            FileWorkManager fileWorkManager, 
            MediaStoreConnector mediaStoreConnector, 
            ElasticAgent elasticAgent, 
            bool doSubFolders,
            bool createBlobFilesIfNew,
            IMessageOutput messageOutput,
            CancellationToken cancellationToken)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<RelateExternalFilesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelateExternalFilesResult()
                };

                folderObject.Additional1 = fileWorkManager.GetPathFileSystemObject(folderObject);
                if (!Directory.Exists(folderObject.Additional1))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The folder {folderObject.Name} is not existing!";
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Get files of {folderObject.Additional1} in database ...");
                var getFilesOfFolderResult = await elasticAgent.GetFilesOfFolders(new List<clsOntologyItem> { folderObject });
                result.ResultState = getFilesOfFolderResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Have files of folder.");

                messageOutput?.OutputInfo($"Check folder {folderObject.Additional1}...");

                var files = Directory.GetFiles(folderObject.Additional1).ToList();
                var fileCount = files.Count;
                var filesDone = 0;

                messageOutput?.OutputInfo($"Found {fileCount} files");

                foreach (var file in files)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        result.ResultState.Additional1 = "Use has cancelled!";
                        messageOutput?.OutputInfo(result.ResultState.Additional1);
                        return result;
                    }
                    if (filesDone % 10 == 0)
                    {
                        messageOutput?.OutputInfo($"Done {filesDone} / {fileCount} files.");
                    }
                    var hashValue = mediaStoreConnector.GetHashOfFile(file);
                    var fileItem = mediaStoreConnector.GetFileOfHash(hashValue);
                    if (fileItem.OList_Rel == null || !fileItem.OList_Rel.Any())
                    {
                        var fileName = Path.GetFileName(file);
                        var fileDb = getFilesOfFolderResult.Result.FirstOrDefault(fileItm => fileItm.Name_Object == fileName);
                        if (fileDb == null)
                        {
                            var fileItemToSave = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = fileName,
                                GUID_Parent = Config.LocalData.Class_File.GUID,
                                Type = Globals.Type_Object
                            };

                            var saveFileToFolderResult = await elasticAgent.SaveNewFileItem(fileItemToSave, folderObject);
                            result.ResultState = saveFileToFolderResult;
                            result.Result.FileItems.Add(fileItemToSave);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                messageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }

                            if (createBlobFilesIfNew)
                            {
                                result.ResultState = mediaStoreConnector.SaveFileToManagedMedia(fileItemToSave, file);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    messageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }

                            }
                            else
                            {
                                result.ResultState = await mediaStoreConnector.SaveHash(fileItemToSave, hashValue);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    messageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }
                            }
                        }
                        else
                        {
                            var fileItm = new clsOntologyItem
                            {
                                GUID = fileDb.ID_Object,
                                Name = fileDb.Name_Object,
                                GUID_Parent = fileDb.ID_Parent_Object,
                                Type = Globals.Type_Object
                            };

                            if (createBlobFilesIfNew)
                            {
                                result.ResultState = mediaStoreConnector.SaveFileToManagedMedia(fileItm, file);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    messageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }

                            }
                            else
                            {
                                result.ResultState = await mediaStoreConnector.SaveHash(fileItm, hashValue);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    messageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        var fileHash = fileItem.OList_Rel.First();

                        var fileDb = getFilesOfFolderResult.Result.FirstOrDefault(fileItm => fileItm.ID_Object == fileHash.GUID);

                        if (fileDb == null)
                        {
                            var saveResult = await SaveFileToFolder(folderObject, fileHash);
                            result.ResultState = saveResult.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                messageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                        }
                    }

                    filesDone++;
                }

                messageOutput?.OutputInfo($"Done {filesDone} / {fileCount} files.");

                if (doSubFolders)
                {
                    var searchSubFolders = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = folderObject.GUID,
                            ID_RelationType = Config.LocalData.ClassRel_Folder_is_subordinated_Folder.ID_RelationType,
                            ID_Parent_Object = Config.LocalData.ClassRel_Folder_is_subordinated_Folder.ID_Class_Left
                        }
                    };

                    var subFoldersResult = await elasticAgent.GetRelations(searchSubFolders);
                    result.ResultState = subFoldersResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Subfolders!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var subFolders = subFoldersResult.Result.Select(fold => new clsOntologyItem
                    {
                        GUID = fold.ID_Object,
                        Name = fold.Name_Object,
                        GUID_Parent = fold.ID_Parent_Object,
                        Type = Globals.Type_Object
                    }).ToList();

                    var subFoldersFileSystem = Directory.GetDirectories(folderObject.Additional1);
                    foreach (var subFolder in subFoldersFileSystem)
                    {
                        var folderName = System.IO.Path.GetFileName(subFolder);
                        var subFolderDb = subFolders.FirstOrDefault(subfold => subfold.Name == folderName);
                        if (subFolderDb == null)
                        {
                            subFolderDb = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = folderName,
                                GUID_Parent = Config.LocalData.Class_Folder.GUID,
                                Type = Globals.Type_Object
                            };
                            var saveSubFolderResult = await elasticAgent.SaveNewSubFolderItem(folderObject, subFolderDb);
                            result.ResultState = saveSubFolderResult;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                messageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                        }
                        var subFolderCompareResult = await CompareFileItems(subFolderDb, 
                            fileWorkManager, 
                            mediaStoreConnector, 
                            elasticAgent, 
                            doSubFolders, 
                            createBlobFilesIfNew,
                            messageOutput,
                            cancellationToken);
                        result.ResultState = subFolderCompareResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            messageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        result.Result.FileItems.AddRange(subFolderCompareResult.Result.FileItems);
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectRel>> SaveFileToFolder(clsOntologyItem folderItem, clsOntologyItem fileItem)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<clsObjectRel>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var relationConfig = new clsRelationConfig(Globals);

                var relToSave = relationConfig.Rel_ObjectRelation(fileItem, folderItem, Config.LocalData.RelationType_is_subordinated);
                var dbWriter = new OntologyModDBConnector(Globals);

                result.ResultState = dbWriter.SaveObjRel(new List<clsObjectRel>
                {
                    relToSave
                });

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the relation between file-item and folder-item!";
                    return result;
                }

                result.Result = relToSave;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportOntologyVersionsResult>> ExportOntologyVersions(ExportOntologyVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExportOntologyVersionsResult>>(async () =>
           {
               var result = new ResultItem<ExportOntologyVersionsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ExportOntologyVersionsResult()
               };

               if (!string.IsNullOrEmpty(request.PathToExportTo))
               {
                   if (!Directory.Exists(request.PathToExportTo))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "The requested path does not exist!";
                       return result;
                   }
               }


               var elasticAgent = new ElasticAgent(Globals);

               var getModelResult = await elasticAgent.GetUpdateOntologyModel();

               result.ResultState = getModelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Ontology-Versions!";
                   return result;
               }

               var versions = GetOntologyVersionsOfModel(getModelResult.Result);

               var versionsToDo = versions;
               if (request.NamespacesToExport.Any())
               {
                   versionsToDo = (from version in versions
                                   join namespaceName in request.NamespacesToExport on version.Namespace equals namespaceName
                                   select version).ToList();
               }

               var namespaces = versionsToDo.GroupBy(version => version.Namespace).Select(namespaceName => namespaceName.Key);

               var exportController = new GenerateOntologyAssemblyController(Globals);

               foreach (var namespaceName in namespaces)
               {

                   var lastVersion = versionsToDo.OrderByDescending(version => version.Major).
                   ThenByDescending(version => version.Minor).
                   ThenByDescending(version => version.Build).
                   ThenByDescending(version => version.Revision).FirstOrDefault();

                   if (lastVersion != null)
                   {
                       var pathToExport = lastVersion.Path;
                       if (!string.IsNullOrEmpty(request.PathToExportTo))
                       {
                           pathToExport = Path.Combine(request.PathToExportTo, Path.GetFileName(lastVersion.Path));
                       }

                       var version = $"{lastVersion.Major}.{lastVersion.Minor}.{lastVersion.Build}.{lastVersion.Revision + 1}";
                       var exportRequest = new GenerateAssemblyRequest(lastVersion.IdOntology, version);
                       var exportResult = await exportController.GenerateAssembly(exportRequest);

                   }
               }

               return result;
           });

            return taskResult;
        }

        private void OutputMessage(IMessageOutput messageOutput, string message, MessageOutputType messageType = MessageOutputType.Info)
        {
            if (messageOutput == null) return;

            messageOutput.OutputMessage(message, messageType);
        }

        public async Task<ResultItem<UpdateOntologyGraphResult>> UpdateOntologyGraph(UpdateAssembliesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<UpdateOntologyGraphResult>>(async () =>
            {
                var result = new ResultItem<UpdateOntologyGraphResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new UpdateOntologyGraphResult()
                };

                var elasticAgent = new ElasticAgent(Globals);

                request.MessageOutput?.OutputInfo("Get Model...");
                var modelResult = await elasticAgent.GetUpdateOntologyModel();
                request.MessageOutput?.OutputInfo("Have Model.");

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Get Versions...");
                var versions = GetOntologyVersionsOfModel(modelResult.Result);
                request.MessageOutput?.OutputInfo($"Have Versions: {versions.Count}");

                var execDir = AppDomain.CurrentDomain.BaseDirectory;

                if (!string.IsNullOrEmpty(request.Path))
                {
                    execDir = request.Path;
                }


                if (request.UseDbPaths)
                {
                    request.MessageOutput?.OutputInfo($"Use Db-Paths");
                    var namespaces = versions.GroupBy(version => version.Namespace).Select(versionGrp => versionGrp.Key).ToList();
                    request.MessageOutput?.OutputInfo($"Namespaces: {namespaces.Count}");

                    foreach (var namespaceItem in namespaces)
                    {
                        request.MessageOutput?.OutputInfo($"Namespace: {namespaceItem}");
                        var version = versions.Where(ver => ver.Namespace == namespaceItem).OrderByDescending(ver => ver.Major).ThenByDescending(ver => ver.Minor).ThenByDescending(ver => ver.Build).ThenByDescending(ver => ver.Revision).FirstOrDefault();

                        if (version != null)
                        {
                            var updateResult = await UpdateOntologiesByFile(version.Path, elasticAgent, versions, request.Test, request.MessageOutput);
                            result.Result.OntologyVersions.Add(updateResult);
                            if (updateResult.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(updateResult.ResultState.Additional1);
                            }
                            else
                            {
                                request.MessageOutput?.OutputInfo($"Done");
                            }
                        }
                    }
                }
                else
                {
                    request.MessageOutput?.OutputInfo($"Don't use Db-Paths");
                    foreach (var strFile in Directory.GetFiles(execDir, "*.dll", SearchOption.AllDirectories))
                    {

                        if (request.ExcludeRegex != null && request.ExcludeRegex.Match(strFile).Success) continue;
                        if (request.IncludeRegex != null && !request.IncludeRegex.Match(strFile).Success) continue;
                        request.MessageOutput?.OutputInfo($"File: {strFile}");
                        var updateResult = await UpdateOntologiesByFile(strFile, elasticAgent, versions, request.Test, request.MessageOutput);
                        result.Result.OntologyVersions.Add(updateResult);
                        if (updateResult.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(updateResult.ResultState.Additional1);
                        }
                        else
                        {
                            request.MessageOutput?.OutputInfo($"Done");
                        }
                    }
                }


                return result;
            });

            return taskResult;
        }

        public List<OntologyVersion> GetOntologyVersionsOfModel(GetUpdateOntologyModelResult modelResult)
        {
            var versions = (from ontologyVersion in modelResult.OntologieVersions
                            join major in modelResult.OntologyVersionAttribs.Where(attr => attr.ID_AttributeType == Config.LocalData.AttributeType_Major.GUID) on ontologyVersion.GUID equals major.ID_Object
                            join minor in modelResult.OntologyVersionAttribs.Where(attr => attr.ID_AttributeType == Config.LocalData.AttributeType_Minor.GUID) on ontologyVersion.GUID equals minor.ID_Object
                            join build in modelResult.OntologyVersionAttribs.Where(attr => attr.ID_AttributeType == Config.LocalData.AttributeType_Build.GUID) on ontologyVersion.GUID equals build.ID_Object
                            join revision in modelResult.OntologyVersionAttribs.Where(attr => attr.ID_AttributeType == Config.LocalData.AttributeType_Revision.GUID) on ontologyVersion.GUID equals revision.ID_Object
                            join path in modelResult.OntologyVersionsToPaths on ontologyVersion.GUID equals path.ID_Object
                            join namespaceItem in modelResult.OntologyVersionsToNameSpaces on ontologyVersion.GUID equals namespaceItem.ID_Object
                            join ontology in modelResult.OntologyVersionsToOntologies on ontologyVersion.GUID equals ontology.ID_Object
                            select new OntologyVersion
                            {
                                IdAttributeMajor = major.ID_Attribute,
                                Major = major.Val_Lng.Value,
                                IdAttributeMinor = minor.ID_Attribute,
                                Minor = minor.Val_Lng.Value,
                                IdAttributeBuild = build.ID_Attribute,
                                Build = build.Val_Lng.Value,
                                IdAttributeRevision = revision.ID_Attribute,
                                Revision = revision.Val_Lng.Value,
                                IdPath = path.ID_Other,
                                Path = path.Name_Other,
                                IdNamespace = namespaceItem.ID_Other,
                                Namespace = namespaceItem.Name_Other,
                                IdOntology = ontology.ID_Other,
                                Ontology = ontology.Name_Other,
                                IdOntologyVersion = ontologyVersion.GUID,
                                OntologyVersionName = ontologyVersion.Name
                            }).ToList();

            return versions;
        }

        private async Task<ResultItem<OntologyVersion>> UpdateOntologiesByFile(string fileName, ElasticAgent elasticAgent, List<OntologyVersion> versions, bool test, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<OntologyVersion>>(async () =>
            {


                var assemblyImporter = new AssemblyImporter(Globals);

                var result = new ResultItem<OntologyVersion>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                try
                {
                    messageOutput?.OutputInfo($"Load File: {fileName}");
                    var objAssembly = Assembly.LoadFile(fileName);

                    var checkAssemblyResult = await assemblyImporter.CheckAssembly(objAssembly);

                    if (checkAssemblyResult.ResultState.GUID == Globals.LState_Success.GUID)
                    {
                        messageOutput?.OutputInfo($"Loaded File: {fileName}");
                        var version = objAssembly.GetName().Version;
                        var types = objAssembly.GetTypes();

                        var namespaceString = types.Select(typeItm => typeItm.Namespace).First();
                        var idOntology = string.Empty;
                        object[] attributes =
                            objAssembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                        if (attributes.Length > 0)
                        {
                            AssemblyDescriptionAttribute descriptionAttribute =
                                (AssemblyDescriptionAttribute)attributes[0];
                            idOntology = descriptionAttribute.Description;
                        }

                        messageOutput?.OutputInfo($"Found Version: {version.Major}.{version.Minor}.{version.Build}.{version.Revision}");

                        if (!string.IsNullOrEmpty(idOntology) && Globals.is_GUID(idOntology) && !string.IsNullOrEmpty(namespaceString) && (!versions.Any() || !versions.Any(dbVersion => dbVersion.Major == version.Major &&
                            dbVersion.Minor == version.Minor &&
                            dbVersion.Build == version.Build &&
                            dbVersion.Revision == version.Revision &&
                            dbVersion.Namespace == namespaceString)))
                        {
                            messageOutput?.OutputInfo($"Import Version...");
                            result.Result = new OntologyVersion()
                            {
                                Path = fileName,
                                Namespace = namespaceString,
                                Major = version.Major,
                                Minor = version.Minor,
                                Build = version.Build,
                                Revision = version.Revision,
                                IdOntology = idOntology
                            };

                            if (!test)
                            {
                                var resultImport = await assemblyImporter.ImportAssembly(checkAssemblyResult.AssemblyType);
                                result.ResultState = resultImport.ResultState;

                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    return result;
                                }

                                var resultSave = await elasticAgent.SaveOntologyVersion(result.Result);
                                result.ResultState = resultSave;
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    return result;
                                }
                            }
                            else
                            {
                                messageOutput?.OutputInfo($"Only Test");
                            }
                            messageOutput?.OutputInfo($"Imported Version");
                        }
                    }


                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while analyizing the Assembly: {ex.Message}";
                }

                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<GetFileVersionsResult>> GetFileVersions(GetFileVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetFileVersionsResult>>(async () =>
            {
                var result = new ResultItem<GetFileVersionsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetFileVersionsResult()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var getModelResult = await elasticAgent.GetFileversionsModel(request);

                result.ResultState = getModelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var textParserController = new TextParserController(Globals);

                var textParserResult = await textParserController.GetTextParser(getModelResult.Result.TextParser);


                result.ResultState = textParserResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var textParserFieldResult = await textParserController.GetParserFields(new clsOntologyItem
                {
                    GUID = textParserResult.Result.First().IdFieldExtractor,
                    Name = textParserResult.Result.First().NameFieldExtractor,
                    GUID_Parent = textParserResult.Result.First().IdClassFieldExtractor,
                    Type = Globals.Type_Object
                });

                result.ResultState = textParserFieldResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var deleteResult = await textParserController.DeleteIndex(textParserResult.Result.First());

                result.ResultState = deleteResult;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var parseTextRequest = new TextParserModule.Models.ParseTextRequest
                {
                    TextParser = textParserResult.Result.First(),
                    ParserFields = textParserFieldResult.Result
                };

                var cancellationTokenSource = new CancellationTokenSource();

                var resultRead = await textParserController.ParseFiles(parseTextRequest, cancellationTokenSource.Token);

                result.ResultState = resultRead.ResultState;
                if (resultRead.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var dbReaderDict = new clsUserAppDBSelector(Globals.Server, Globals.Port, textParserResult.Result.First().NameIndexElasticSearch, 5000, Globals.Session);

                var query = "*";
                var docResult = dbReaderDict.GetData_Documents(-1, textParserResult.Result.First().NameIndexElasticSearch, textParserResult.Result.First().NameEsType, query);

                if (!docResult.IsOK)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Documents cannot be read!";
                    return result;
                }

                var servers = docResult.Documents.GroupBy(doc => doc.Dict["Hostname"].ToString()).Select(docGrp => new clsOntologyItem
                {
                    Name = docGrp.Key,
                    GUID_Parent = FileVersions.Config.LocalData.Class_Server.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var checkServersResult = await elasticAgent.CheckObjects(servers);

                result.ResultState = checkServersResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var files = docResult.Documents.Select(doc => new clsOntologyItem
                {
                    Name = doc.Dict["File"].ToString(),
                    GUID_Parent = FileVersions.Config.LocalData.Class_File.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var checkFilesResult = await elasticAgent.CheckObjects(files);

                result.ResultState = checkFilesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var environments = docResult.Documents.GroupBy(doc => doc.Dict["Environment"].ToString()).Select(doc => new clsOntologyItem
                {
                    GUID = doc.Key.ToString(),
                    GUID_Parent = FileVersions.Config.LocalData.Class_Environment.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var checkEnvironmentsResult = await elasticAgent.CheckObjects(environments, false);

                result.ResultState = checkEnvironmentsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var versions = docResult.Documents.GroupBy(doc => doc.Dict["Version"].ToString()).Select(doc => new clsOntologyItem
                {
                    Name = doc.Key,
                    GUID_Parent = FileVersions.Config.LocalData.Class_Development_Version.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var checkVersionsResult = await elasticAgent.CheckObjects(versions);

                result.ResultState = checkVersionsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var paths = docResult.Documents.GroupBy(doc => doc.Dict["Path"].ToString()).Select(doc => new clsOntologyItem
                {
                    Name = doc.Key,
                    GUID_Parent = FileVersions.Config.LocalData.Class_Path.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var checkPathsResult = await elasticAgent.CheckObjects(paths);

                result.ResultState = checkPathsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var fileVersions = new List<clsOntologyItem>();
                var fileVersionsToFiles = new List<clsObjectRel>();
                var fileVersionsToLogDates = new List<clsObjectAtt>();
                var fileVersionsToServers = new List<clsObjectRel>();
                var fileVersionsToEnvironments = new List<clsObjectRel>();
                var fileVersionsToVersion = new List<clsObjectRel>();
                var fileVersionsTopath = new List<clsObjectRel>();

                var relationConfig = new clsRelationConfig(Globals);

                foreach (var doc in docResult.Documents)
                {
                    var fileVersionItem = new clsOntologyItem
                    {
                        GUID = MD5Converters.CalculateMD5Hash($"{doc.Dict["Path"]}{doc.Dict["File"]}{doc.Dict["LogDate"]}"),
                        Name = doc.Dict["File"].ToString(),
                        GUID_Parent = FileVersions.Config.LocalData.Class_Fileversions.GUID,
                        Type = Globals.Type_Object
                    };

                    fileVersions.Add(fileVersionItem);

                    var fileVersionToLogDate = relationConfig.Rel_ObjectAttribute(fileVersionItem, FileVersions.Config.LocalData.AttributeType_DateTimestamp, doc.Dict["LogDate"]);

                    fileVersionsToLogDates.Add(fileVersionToLogDate);

                    var file = checkFilesResult.Result.FirstOrDefault(fileItm => fileItm.Name == doc.Dict["File"].ToString());

                    if (file == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"File {doc.Dict["File"].ToString()} couldn't be found!";
                        return result;
                    }

                    fileVersionsToFiles.Add(relationConfig.Rel_ObjectRelation(fileVersionItem, file, FileVersions.Config.LocalData.RelationType_belongs_to));

                    var server = checkServersResult.Result.FirstOrDefault(serverItm => serverItm.Name == doc.Dict["Hostname"].ToString());

                    if (server == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Server {doc.Dict["Hostname"].ToString()} couldn't be found!";
                        return result;
                    }

                    fileVersionsToServers.Add(relationConfig.Rel_ObjectRelation(fileVersionItem, server, FileVersions.Config.LocalData.RelationType_belongs_to));

                    var environment = checkEnvironmentsResult.Result.FirstOrDefault(environ => environ.GUID == doc.Dict["Environment"].ToString());

                    if (environment == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Environment {doc.Dict["Environment"].ToString()} couldn't be found!";
                        return result;
                    }

                    fileVersionsToEnvironments.Add(relationConfig.Rel_ObjectRelation(fileVersionItem, environment, FileVersions.Config.LocalData.RelationType_belongs_to));

                    var versionItem = checkVersionsResult.Result.FirstOrDefault(version => version.Name == doc.Dict["Version"].ToString());

                    if (versionItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Version {doc.Dict["Version"].ToString()} couldn't be found!";
                        return result;
                    }

                    fileVersionsToVersion.Add(relationConfig.Rel_ObjectRelation(fileVersionItem, versionItem, FileVersions.Config.LocalData.RelationType_belongs_to));

                    var pathItem = checkPathsResult.Result.FirstOrDefault(path => path.Name == doc.Dict["Path"].ToString());

                    if (pathItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Path {doc.Dict["Path"].ToString()} couldn't be found!";
                        return result;
                    }

                    fileVersionsTopath.Add(relationConfig.Rel_ObjectRelation(fileVersionItem, pathItem, FileVersions.Config.LocalData.RelationType_belongs_to));
                }

                var checkResult = await elasticAgent.CheckObjects(fileVersions, false);

                var checkFileVersionsToAttributes = await elasticAgent.CheckRelations(fileVersionsToLogDates);
                result.ResultState = checkFileVersionsToAttributes.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var checkFileVersionsToFiles = await elasticAgent.CheckRelations(fileVersionsToFiles);
                result.ResultState = checkFileVersionsToFiles.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var checkFileVersionsToServers = await elasticAgent.CheckRelations(fileVersionsToServers);
                result.ResultState = checkFileVersionsToServers.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var checkFileVersionsToEnvironments = await elasticAgent.CheckRelations(fileVersionsToEnvironments);
                result.ResultState = checkFileVersionsToEnvironments.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = checkResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var checkFileVersionsToVersions = await elasticAgent.CheckRelations(fileVersionsToVersion);
                result.ResultState = checkFileVersionsToVersions.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var checkFileVersionsToPaths = await elasticAgent.CheckRelations(fileVersionsTopath);
                result.ResultState = checkFileVersionsToPaths.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });


            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetFileSystemItemPath(clsOntologyItem fileSystemItem)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = Globals.LState_Success.Clone()
                };



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<BackupRelatedFilesResult>> BackupRelatedFiles(BackupRelatedFilesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<BackupRelatedFilesResult>>(async () =>
            {
                var result = new ResultItem<BackupRelatedFilesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new BackupRelatedFilesResult()
                };

                var serviceAgent = new Services.ElasticAgent(Globals);

                request.MessageOutput?.OutputInfo("Getting model...");
                var getModelResult = await serviceAgent.GetBackupRelatedFilesModel(request);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                var typedTaggingController = new TypedTaggingConnector(Globals);

                foreach (var configItem in getModelResult.Result.ConfigItems)
                {
                    if (request.CancellationToken.IsCancellationRequested)
                    {
                        result.ResultState.Additional1 = "Cancelled by User";
                        return result;
                    }
                    var configResult = new ConfigResult
                    {
                        ConfigItem = configItem,
                        ResultState = Globals.LState_Success.Clone()
                    };
                    result.Result.ConfigResults.Add(configResult);

                    var path = getModelResult.Result.Paths.SingleOrDefault(rel => rel.ID_Object == configItem.GUID);
                    if (path == null)
                    {
                        configResult.ResultState = Globals.LState_Error.Clone();
                        configResult.ResultState.Additional1 = $"No path is related to config {configItem.Name}";
                        request.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                        continue;
                    }

                    var relationLevel = getModelResult.Result.RelationLevels.Single(rel => rel.ID_Object == configItem.GUID);
                    if (relationLevel == null)
                    {
                        configResult.ResultState = Globals.LState_Error.Clone();
                        configResult.ResultState.Additional1 = $"No relation-level is defined for config {configItem.Name}";
                        request.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                        continue;
                    }

                    var objects = getModelResult.Result.Objects.Where(obj => obj.ID_Object == configItem.GUID).ToList();

                    if (!objects.Any())
                    {
                        configResult.ResultState = Globals.LState_Error.Clone();
                        configResult.ResultState.Additional1 = $"No objects related to config {configItem.Name}";
                        request.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                        continue;
                    }

                    if (!System.IO.Directory.Exists(path.Name_Other))
                    {
                        configResult.ResultState = Globals.LState_Error.Clone();
                        configResult.ResultState.Additional1 = $"The Path {path.Name_Other} of config {configItem.Name} does not exist!";
                        request.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                        continue;
                    }

                    var filesResult = await GetFiles(objects.Select(obj => new clsOntologyItem
                    {
                        GUID = obj.ID_Other,
                        Name = obj.Name_Other,
                        GUID_Parent = obj.ID_Parent_Other,
                        Type = obj.Ontology
                    }).ToList(),
                        request.MessageOutput,
                        configItem, relationLevel.Val_Lng.Value, 1);

                    configResult.ResultState = filesResult.ResultState;
                    if (configResult.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        continue;
                    }

                    var fileWorkManager = new FileWorkManager(Globals);

                    var files = filesResult.Result.Select(file => new { file, IsBlobFile = fileWorkManager.IsBlobFile(file) });

                    var blobFiles = files.Where(file => file.IsBlobFile);
                    var nonBlobFiles = files.Where(file => !file.IsBlobFile);

                    var mediaStoreController = new MediaStoreConnector(Globals);

                    var blobStreams = blobFiles.Select(file => new { file.file, stream = mediaStoreController.GetManagedMediaStream(file.file) });

                    var filePaths = nonBlobFiles.Select(file => new { file.file, path = fileWorkManager.GetPathFileSystemObject(file.file) });

                    foreach (var blobStream in blobStreams)
                    {
                        var filePath = Path.Combine(path.Name_Other, blobStream.file.Name);

                        try
                        {
                            using (var streamWriter = new FileStream(filePath, FileMode.Create))
                            {
                                blobStream.stream.CopyTo(streamWriter);
                            }
                            blobStream.stream.Close();


                        }
                        catch (Exception ex)
                        {
                            configResult.ResultState = BackupRelated.Config.LocalData.Object_Warning;
                            configResult.ResultState.Additional1 = $"Error while saving blob-file: {ex.Message}";

                        }
                    }

                    foreach (var file in filePaths)
                    {
                        var filePath = Path.Combine(path.Name_Other, file.file.Name);
                        try
                        {
                            System.IO.File.Copy(file.path, filePath, true);
                        }
                        catch (Exception ex)
                        {

                            configResult.ResultState = BackupRelated.Config.LocalData.Object_Warning;
                            configResult.ResultState.Additional1 = $"Error while saving blob-file: {ex.Message}";
                        }
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetFilesOfFolders(GetFilesOfFoldersRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(async () =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var elasticAgent = new ElasticAgent(Globals);
               request.MessageOutput?.OutputInfo($"Search files of {request.Folders.Count} folders...");
               var searchFilesResult = await elasticAgent.GetFilesOfFolders(request.Folders);

               result.ResultState = searchFilesResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Found {searchFilesResult.Result.Count} files of {request.Folders.Count} folders.");
               result.Result = searchFilesResult.Result;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetFiles(
            List<clsOntologyItem> objects,
            IMessageOutput messageOutput,
            clsOntologyItem configItem,
            long relLevel,
            long maxRelLevel)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
             {
                 var serviceAgent = new ElasticAgent(Globals);
                 var typedTaggingController = new TypedTaggingConnector(Globals);

                 var result = new ResultItem<List<clsOntologyItem>>
                 {
                     ResultState = Globals.LState_Success.Clone(),
                     Result = new List<clsOntologyItem>()
                 };

                 var searchFiles = objects.Select(obj => new clsObjectRel
                 {
                     ID_Object = obj.GUID
                 }).ToList();
                 searchFiles.AddRange(objects.Select(obj => new clsObjectRel
                 {
                     ID_Other = obj.GUID,
                     ID_Parent_Object = BackupRelated.Config.LocalData.Class_File.GUID
                 }));

                 var getFilesResult = await serviceAgent.GetRelations(searchFiles);

                 if (getFilesResult.ResultState.GUID == Globals.LState_Error.GUID)
                 {
                     result.ResultState = Globals.LState_Error.Clone();
                     result.ResultState.Additional1 = $"Error while getting file-relations of {configItem.Name}";
                     messageOutput?.OutputError(result.ResultState.Additional1);
                     return result;
                 }

                 result.Result.AddRange(getFilesResult.Result.Where(rel => rel.ID_Parent_Other == BackupRelated.Config.LocalData.Class_File.GUID || rel.ID_Parent_Object == BackupRelated.Config.LocalData.Class_File.GUID).Select(rel => new clsOntologyItem
                 {
                     GUID = rel.ID_Parent_Object == BackupRelated.Config.LocalData.Class_File.GUID ? rel.ID_Object : rel.ID_Other,
                     Name = rel.ID_Parent_Object == BackupRelated.Config.LocalData.Class_File.GUID ? rel.Name_Object : rel.Name_Other,
                     GUID_Parent = BackupRelated.Config.LocalData.Class_File.GUID,
                     Type = Globals.Type_Object
                 }));

                 var tags = new List<clsOntologyItem>();
                 foreach (var objectItem in objects)
                 {
                     var getTagsResult = await typedTaggingController.GetTags(objectItem);
                     if (getTagsResult.Result.GUID == Globals.LState_Error.GUID)
                     {
                         result.ResultState = Globals.LState_Error.Clone();
                         result.ResultState.Additional1 = $"Error while getting tags of {configItem.Name}";
                         messageOutput?.OutputError(result.ResultState.Additional1);
                         return result;
                     }

                     tags.AddRange(getTagsResult.TypedTags.Select(typedTag => new clsOntologyItem
                     {
                         GUID = typedTag.IdTag,
                         Name = typedTag.NameTag,
                         GUID_Parent = typedTag.IdTagParent,
                         Type = typedTag.TagType
                     }));
                 }

                 var fileTags = tags.Where(tag => tag.GUID_Parent == BackupRelated.Config.LocalData.Class_File.GUID).ToList();
                 var nonFileTags = tags.Where(tag => tag.GUID_Parent != BackupRelated.Config.LocalData.Class_File.GUID).ToList();

                 result.Result.AddRange(fileTags);

                 if (relLevel < maxRelLevel)
                 {
                     var nonFileRels = getFilesResult.Result.Where(rel => rel.ID_Parent_Object != BackupRelated.Config.LocalData.Class_File.GUID
                        && rel.ID_Parent_Other != BackupRelated.Config.LocalData.Class_File.GUID);
                     var nonFileObjects = (from fileResult in nonFileRels
                                           join objRelLeftRight in objects on fileResult.ID_Object equals objRelLeftRight.GUID
                                           select new clsOntologyItem
                                           {
                                               GUID = fileResult.ID_Other,
                                               Name = fileResult.Name_Other,
                                               GUID_Parent = fileResult.ID_Parent_Other,
                                               Type = fileResult.Ontology
                                           }).ToList();

                     nonFileObjects.AddRange(from fileResult in nonFileRels
                                             join objRelLeftRight in objects on fileResult.ID_Other equals objRelLeftRight.GUID
                                             select new clsOntologyItem
                                             {
                                                 GUID = fileResult.ID_Object,
                                                 Name = fileResult.Name_Object,
                                                 GUID_Parent = fileResult.ID_Parent_Object,
                                                 Type = fileResult.Ontology
                                             });

                     nonFileObjects.AddRange(nonFileTags);

                     if (nonFileObjects.Any())
                     {
                         var getSubFilesResult = await GetFiles(nonFileObjects, messageOutput, configItem, relLevel + 1, maxRelLevel);
                         result.ResultState = getSubFilesResult.ResultState;
                         if (result.ResultState.GUID == Globals.LState_Error.GUID)
                         {
                             return result;
                         }

                         result.Result.AddRange(getSubFilesResult.Result);
                     }
                 }

                 return result;
             });

            return taskResult;
        }

        public async Task<ResultItem<ImportFileAttributesResult>> ImportFileAttributes(ImportFileAttributesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ImportFileAttributesResult>>(async () =>
           {
               var result = new ResultItem<ImportFileAttributesResult>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               var serviceAgent = new ElasticAgent(Globals);

               OutputMessage(request.MessageOutput, "Get Model....Started");
               var modelResult = await serviceAgent.GetFileAttributesModel(request);
               OutputMessage(request.MessageOutput, "Get Model....Finished");
               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var textParserController = new TextParserController(Globals);

               OutputMessage(request.MessageOutput, $"Get Textparser...Started");
               var getTextParserResult = await textParserController.GetTextParser(modelResult.Result.TextParser);
               OutputMessage(request.MessageOutput, $"Get Textparser...Finished");

               result.ResultState = getTextParserResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var textParser = getTextParserResult.Result.First();

               if (!getTextParserResult.Result.Any())
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The provided Textparser is not valid!";
                   return result;
               }

               var path = modelResult.Result.Path.Name;

               var dbSelector = new clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
               var dbWriter = new clsUserAppDBUpdater(dbSelector);

               dbSelector.DeleteIndex(textParser.NameIndexElasticSearch);

               var fileDocs = new List<clsAppDocuments>();
               var folderDocs = new List<clsAppDocuments>();

               var importFileRequest = new ImportFileRequest
               {
                   Path = path,
                   FileDocs = fileDocs,
                   FolderDocs = folderDocs,
                   DbWriter = dbWriter,
                   TextParser = textParser,
                   GetCreateTime = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_CreateTime.GUID),
                   GetFileCount = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FileCount.GUID),
                   GetFileFullName = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FileFullName.GUID),
                   GetFileName = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FileName.GUID),
                   GetFilePath = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FilePath.GUID),
                   GetFileSize = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FileSize.GUID),
                   GetLastWriteTime = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_LastWriteTime.GUID),
                   GetVersion = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_Version.GUID),
                   GetFileCountRec = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_FileCountRec.GUID),
                   GetExtension = modelResult.Result.ConfigsToFileAttributes.Any(attr => attr.ID_Other == FileAttributes.Config.LocalData.Object_Extension.GUID),
                   MessageOutput = request.MessageOutput
               };

               result = ImportFiles(importFileRequest);

               if (fileDocs.Count > 0)
               {
                   result.Result.FileCount += fileDocs.Count;
                   dbWriter.SaveDoc(fileDocs, textParser.NameEsType);
                   fileDocs.Clear();
                   OutputMessage(request.MessageOutput, $"Files imported: {result.Result.FileCount}");
               }

               if (folderDocs.Count > 0)
               {
                   dbWriter.SaveDoc(folderDocs, textParser.NameEsType);
                   folderDocs.Clear();
                   OutputMessage(request.MessageOutput, $"Folders imported: {result.Result.FolderCount}");
               }

               return result;
           });

            return taskResult;
        }

        private ResultItem<ImportFileAttributesResult> ImportFiles(ImportFileRequest request)
        {
            var result = new ResultItem<ImportFileAttributesResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new ImportFileAttributesResult()
            };


            try
            {
                var dictFolder = new Dictionary<string, object>();
                var filesCount = 0;

                dictFolder.Add(FileAttributes.Config.LocalData.Object_FilePath.Name, request.Path);
                result.Result.FolderCount++;
                try
                {
                    OutputMessage(request.MessageOutput, $"Search files in Directory: {request.Path}");
                    var files = Directory.GetFiles(request.Path, "*", SearchOption.TopDirectoryOnly);
                    filesCount = files.Count();
                    OutputMessage(request.MessageOutput, $"Found {files.Length} files in Directory: {request.Path}");
                    result.Result.FileCount = files.Length;
                    foreach (var file in files)
                    {

                        var fileInfo = new FileInfo(file);
                        result.Result.FileSize += fileInfo.Length;

                        var dict = new Dictionary<string, object>();

                        if (request.GetFileName)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_FileName.Name, fileInfo.Name);
                        }
                        if (request.GetFilePath)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_FilePath.Name, Path.GetDirectoryName(file));
                        }
                        if (request.GetFileFullName)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_FileFullName.Name, file);
                        }
                        if (request.GetFileSize)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_FileSize.Name, fileInfo.Length);
                        }
                        if (request.GetCreateTime)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_CreateTime.Name, fileInfo.CreationTime);
                        }
                        if (request.GetLastWriteTime)
                        {
                            dict.Add(FileAttributes.Config.LocalData.Object_LastWriteTime.Name, fileInfo.LastWriteTime);
                        }
                        if (request.GetVersion)
                        {
                            var fileVersion = FileVersionInfo.GetVersionInfo(file);
                            dict.Add(FileAttributes.Config.LocalData.Object_Version.Name, fileVersion.FileVersion);
                        }
                        if (request.GetExtension)
                        {
                            var extension = Path.GetExtension(file);
                            if (!string.IsNullOrEmpty(extension))
                            {
                                dict.Add(FileAttributes.Config.LocalData.Object_Extension.Name, extension);
                            }
                        }


                        request.FileDocs.Add(new clsAppDocuments { Id = Globals.NewGUID, Dict = dict });


                        if (request.FileDocs.Count > 5000)
                        {
                            result.Result.FileCount += request.FileDocs.Count;
                            request.DbWriter.SaveDoc(request.FileDocs, request.TextParser.NameEsType);
                            request.FileDocs.Clear();
                            OutputMessage(request.MessageOutput, $"Files imported: {result.Result.FileCount}");
                        }


                    }


                    foreach (var subPath in Directory.GetDirectories(request.Path))
                    {
                        var subrequest = new ImportFileRequest
                        {
                            Path = subPath,
                            DbWriter = request.DbWriter,
                            TextParser = request.TextParser,
                            GetCreateTime = request.GetCreateTime,
                            GetFileCount = request.GetFileCount,
                            GetFileFullName = request.GetFileFullName,
                            GetFileName = request.GetFileName,
                            GetFilePath = request.GetFilePath,
                            GetFileSize = request.GetFileSize,
                            GetLastWriteTime = request.GetLastWriteTime,
                            GetVersion = request.GetVersion,
                            GetFileCountRec = request.GetFileCountRec,
                            FileDocs = request.FileDocs,
                            FolderDocs = request.FolderDocs,
                            GetExtension = request.GetExtension,
                            MessageOutput = request.MessageOutput
                        };
                        var subResult = ImportFiles(subrequest);
                        result.ResultState = subResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.FileCount += subResult.Result.FileCount;
                        result.Result.FileSize += subResult.Result.FileSize;
                        result.Result.FolderCount += subResult.Result.FolderCount;
                    }

                    dictFolder.Add(FileAttributes.Config.LocalData.Object_FileSize.Name, result.Result.FileSize);
                    dictFolder.Add(FileAttributes.Config.LocalData.Object_FileCount.Name, filesCount);
                    dictFolder.Add(FileAttributes.Config.LocalData.Object_FileCountRec.Name, result.Result.FileCount);

                    if (request.GetFilePath)
                    {
                        var folderInfo = new DirectoryInfo(request.Path);
                        if (request.GetLastWriteTime)
                        {
                            dictFolder.Add(FileAttributes.Config.LocalData.Object_LastWriteTime.Name, folderInfo.LastWriteTime);
                        }

                        if (request.GetCreateTime)
                        {
                            dictFolder.Add(FileAttributes.Config.LocalData.Object_CreateTime.Name, folderInfo.CreationTime);
                        }

                        request.FolderDocs.Add(new clsAppDocuments { Id = Globals.NewGUID, Dict = dictFolder });

                        if (request.FolderDocs.Count > 5000)
                        {
                            result.Result.FolderCount += request.FolderDocs.Count;
                            request.DbWriter.SaveDoc(request.FolderDocs, request.TextParser.NameEsType);
                            request.FolderDocs.Clear();
                            OutputMessage(request.MessageOutput, $"Folders imported: {result.Result.FolderCount}");
                        }

                    }
                }
                catch (Exception ex)
                {
                    result.Result.ErrorCount++;
                }

            }
            catch (Exception ex)
            {

                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = $"Error: {ex.Message}";
            }

            return result;
        }

        public async Task<ResultItem<CalculateHashForBlobFilesResult>> CalculateHashForBlobFiles(CalculateHashForBlobFilesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CalculateHashForBlobFilesResult>>(async () =>
           {
               var result = new ResultItem<CalculateHashForBlobFilesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new CalculateHashForBlobFilesResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateCalculateHashRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo("Get model...");
               var getModelResult = await elasticAgent.GetModelCalculateHashForBlobFiles(request);

               result.ResultState = getModelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have model. Recalculate: {getModelResult.Result.Recalculate?.Val_Bit.Value.ToString() ?? false.ToString()}, Folderfilter: {getModelResult.Result.FolderFilter.Count} Folders, Filefilter: {getModelResult.Result.FilesFilter.Count} Files.");

               request.MessageOutput?.OutputInfo("Get Files for filter...");
               var getFilesFilter = await elasticAgent.GetFilesFilterForHashing(getModelResult.Result);

               result.ResultState = getFilesFilter.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (getFilesFilter.Result.UseFilter)
               {
                   request.MessageOutput?.OutputInfo($"Have {getFilesFilter.Result.FilesFilter.Count} Files for filter.");
               }
               else
               {
                   request.MessageOutput?.OutputInfo($"No filter.");
               }


               request.MessageOutput?.OutputInfo($"Get files...");
               var getBlobFilesForHashing = await elasticAgent.GetBlobFilesForHashing(getFilesFilter.Result, request.MessageOutput);

               result.ResultState = getBlobFilesForHashing.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {getBlobFilesForHashing.Result.Count} files.");
               var mediaStoreController = new MediaStoreConnector(Globals);

               long fileCount = 0;

               request.MessageOutput?.OutputInfo($"Hash files...");
               foreach (var blobFile in getBlobFilesForHashing.Result)
               {
                   if (request.CancellationToken.IsCancellationRequested)
                   {
                       request.MessageOutput?.OutputWarning("Stopped by user!");
                       break;
                   }
                   var setHashResult = await mediaStoreController.SetHash(new clsOntologyItem
                   {
                       GUID = blobFile.ID_Object,
                       Name = blobFile.Name_Object,
                       GUID_Parent = blobFile.ID_Class,
                       Type = Globals.Type_Object
                   });

                   result.ResultState = setHashResult;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   if (fileCount > 0 && (fileCount % 20 == 0))
                   {
                       request.MessageOutput?.OutputInfo($"Hashed {fileCount} files.");
                   }

                   fileCount++;
               }

               request.MessageOutput?.OutputInfo($"Hashed {fileCount} files.");

               return result;
           });

            return taskResult;
        }
        public async Task<clsOntologyItem> CheckMediaServier()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var getMediaStoreHostResult = await mediaStoreConnector.GetMediaServiceHost();
                if (getMediaStoreHostResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return getMediaStoreHostResult.ResultState;
                }

                var restClient = new RestClient(getMediaStoreHostResult.Result.ServiceUrl);
                var restRequest = new RestRequest("checkMediaServer", Method.GET);

                IRestResponse<bool> response = restClient.Execute<bool>(restRequest);

                var result = response.Data ? Globals.LState_Success.Clone() : Globals.LState_Error.Clone();
                result.Additional1 = "Mediaserver is not running!";
                result.Additional2 = getMediaStoreHostResult.Result.ServiceUrl;
                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<List<ResultItem<clsOntologyItem>>>> UpdateCreateStamp(UpdateCreateStampRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ResultItem<clsOntologyItem>>>>(async () =>
           {
               var result = new ResultItem<List<ResultItem<clsOntologyItem>>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ResultItem<clsOntologyItem>>()
               };

               var mediaStoreConnector = new MediaStoreConnector(Globals);
               var getMediaServiceHost = await CheckMediaServier();

               request.MessageOutput?.OutputInfo("Check Mediaserver...");
               result.ResultState = getMediaServiceHost;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError($"The Media-Server is not running: {getMediaServiceHost.Additional1}");
                   return result;
               }
               request.MessageOutput?.OutputInfo("Checked Mediaserver.");

               request.MessageOutput?.OutputInfo("Validate request...");

               result.ResultState = ValidationController.ValidateUpdateCreateSatmpRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo("Get model...");
               var modelResult = await elasticAgent.GetUpdateCreateStampModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");
               request.MessageOutput?.OutputInfo($"Have {modelResult.Result.Files.Count} files.");

               var fileWorkManager = new FileWorkManager(Globals);

               var client = new RestClient(getMediaServiceHost.Additional2);
               var filesWithStamps = (from fileItem in modelResult.Result.Files
                                      join stamp in modelResult.Result.CreateStamps on fileItem.GUID equals stamp.ID_Object into stamps
                                      from stamp in stamps.DefaultIfEmpty()
                                      select new { fileItem, stamp });

               var attributesToSave = new List<clsObjectAtt>();
               var relationConfig = new clsRelationConfig(Globals);

               request.MessageOutput?.OutputInfo("Iterate through files...");
               foreach (var fileItem in filesWithStamps)
               {
                   var resultItem = new ResultItem<clsOntologyItem>
                   {
                       ResultState = Globals.LState_Success.Clone(),
                       Result = fileItem.fileItem
                   };
                   result.Result.Add(resultItem);
                   var blobFileItem = fileItem.fileItem.Mark.HasValue && fileItem.fileItem.Mark.Value;
                   if (!blobFileItem)
                   {
                       fileItem.fileItem.Additional1 = fileWorkManager.GetPathFileSystemObject(fileItem.fileItem, blobFileItem);
                       try
                       {
                           var fileInfo = new FileInfo(fileItem.fileItem.Additional1);
                           if (fileItem.stamp == null)
                           {
                               attributesToSave.Add(relationConfig.Rel_ObjectAttribute(fileItem.fileItem, Config.LocalData.AttributeType_Datetimestamp__Create_, fileInfo.CreationTime));
                           }
                           else if (fileItem.stamp.Val_Datetime != fileInfo.CreationTime)
                           {
                               fileItem.stamp.Val_Datetime = fileInfo.CreationTime;
                               attributesToSave.Add(fileItem.stamp);
                           }
                       }
                       catch (Exception ex)
                       {
                           resultItem.ResultState = Globals.LState_Error.Clone();
                           resultItem.ResultState.Additional1 = ex.Message;
                       }
                   }
                   else
                   {

                       var mediaStoreRequest = new RestRequest("getCreateStamp", Method.POST);
                       mediaStoreRequest.AddParameter("idFile", fileItem.fileItem.GUID); // adds to POST or URL querystring based on Method

                       IRestResponse<MediaItemPathMap> response2 = client.Execute<MediaItemPathMap>(mediaStoreRequest);
                       if (!response2.IsSuccessful)
                       {
                           request.MessageOutput?.OutputError($"The Mediaitem cannot be exported: {response2.ErrorMessage}");
                       }
                       else
                       {
                           var resultListToCheck = filesWithStamps.FirstOrDefault(item => item.fileItem.GUID == response2.Data.IdFile);

                           if (resultListToCheck.stamp == null)
                           {
                               attributesToSave.Add(relationConfig.Rel_ObjectAttribute(fileItem.fileItem, Config.LocalData.AttributeType_Datetimestamp__Create_, response2.Data.CreateStamp));
                           }
                           else if (resultListToCheck.stamp.Val_Datetime != response2.Data.CreateStamp)
                           {
                               fileItem.stamp.Val_Datetime = response2.Data.CreateStamp;
                               attributesToSave.Add(fileItem.stamp);
                           }
                       }

                   }
               }

               request.MessageOutput?.OutputInfo("Checked all files...");

               request.MessageOutput?.OutputInfo($"Save {attributesToSave.Count} Attributes...");
               if (attributesToSave.Any())
               {
                   var saveResult = await elasticAgent.SaveAttributes(attributesToSave);
                   result.ResultState = saveResult;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   }
               }
               request.MessageOutput?.OutputInfo($"Saved Attributes.");

               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> ReplaceContent(ReplaceContentRequest request, bool isAspNetProject)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var sessionStamp = DateTime.Now;
                var saveCount = 100;

                request.MessageOutput?.OutputInfo("Validate request...");

                result = ValidationController.ValidateReplaceContentRequest(request, Globals);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get Model...");

                var elasticAgent = new ElasticAgent(Globals);

                var getModelResult = await elasticAgent.GetReplaceContentModel(request);

                result = getModelResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model.");

                var objectsToSave = new List<clsOntologyItem>();
                var attributesToSave = new List<clsObjectAtt>();
                var relationsToSave = new List<clsObjectRel>();
                var relationConfig = new clsRelationConfig(Globals);
                var reportController = new ReportController(Globals, isAspNetProject);
                var commandLineRunController = new CommandLineRunController(Globals);

                foreach (var config in getModelResult.Result.Configs)
                {

                    request.MessageOutput?.OutputInfo($"Config: {config.Name}");

                    var reportReplace = getModelResult.Result.ConfigsToReportReplace.FirstOrDefault(repRep => repRep.ID_Object == config.GUID);
                    if (reportReplace == null)
                    {
                        await ReplaceInFile(elasticAgent,
                            config,
                            getModelResult.Result,
                            objectsToSave,
                            attributesToSave,
                            relationsToSave,
                            relationConfig,
                            request.MessageOutput,
                            sessionStamp,
                            saveCount,
                            request.CancellationToken);
                    }
                    else
                    {
                        await ReplaceByReport(elasticAgent,
                            reportController,
                            commandLineRunController,
                            config,
                            reportReplace,
                            getModelResult.Result,
                            objectsToSave,
                            attributesToSave,
                            relationsToSave,
                            relationConfig,
                            request.MessageOutput,
                            sessionStamp,
                            saveCount,
                            request.CancellationToken);
                    }
                }

                if (objectsToSave.Count > 0)
                {
                    result = await SaveLog(objectsToSave, attributesToSave, relationsToSave, objectsToSave.Count, elasticAgent, request.MessageOutput);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ReplaceByReport(ElasticAgent elasticAgent,
            ReportController reportController,
            CommandLineRunController commandLineRunController,
            clsOntologyItem config,
            clsObjectRel configToReportReplace,
            ReplaceContentModel model,
            List<clsOntologyItem> objectsToSave,
            List<clsObjectAtt> attributesToSave,
            List<clsObjectRel> relationsToSave,
            clsRelationConfig relationConfig,
            IMessageOutput messageOutput,
            DateTime sessionStamp,
            int saveCount,
            CancellationToken cancellationToken)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                messageOutput?.OutputInfo("Getting pre-report, field for path, report-filter, report-sort, command line run (search), command line run (replace) ...");
                var testItem = model.ConfigsTest.FirstOrDefault(test => test.ID_Object == config.GUID);
                var report = model.ReportReplacesToReports.Where(rep => rep.ID_Object == configToReportReplace.ID_Other).Select(rep => new clsOntologyItem
                {
                    GUID = rep.ID_Other,
                    Name = rep.Name_Other,
                    GUID_Parent = rep.ID_Parent_Other,
                    Type = rep.Ontology
                }).FirstOrDefault();


                var fieldPath = model.ReportReplacesToReportFieldFilePaths.Where(fileP => fileP.ID_Object == configToReportReplace.ID_Other).Select(filePath => new clsOntologyItem
                {
                    GUID = filePath.ID_Other,
                    Name = filePath.Name_Other,
                    GUID_Parent = filePath.ID_Parent_Other,
                    Type = filePath.Ontology
                }).FirstOrDefault();
                var reportFilter = model.ReportReplacesToReportFilters.Where(repFilt => repFilt.ID_Object == configToReportReplace.ID_Other).Select(repFilt => new clsOntologyItem
                {
                    GUID = repFilt.ID_Other,
                    Name = repFilt.Name_Other,
                    GUID_Parent = repFilt.ID_Parent_Other,
                    Type = repFilt.Ontology
                }).ToList();

                var reportSort = model.ReportReplacesToReportSorts.Where(repSort => repSort.ID_Object == configToReportReplace.ID_Other).Select(repSort => new clsOntologyItem
                {
                    GUID = repSort.ID_Other,
                    Name = repSort.Name_Other,
                    GUID_Parent = repSort.ID_Parent_Other,
                    Type = repSort.Ontology
                }).ToList();

                var commandLineRunSearchPre = model.ReportReplacesToCommandLineRuns.Where(cmd => cmd.ID_Object == configToReportReplace.ID_Other &&
                    cmd.ID_RelationType == FileManagementModule.ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_search_Comand_Line__Run_.ID_RelationType).
                    Select(cmd => new clsOntologyItem
                    {
                        GUID = cmd.ID_Other,
                        Name = cmd.Name_Other,
                        GUID_Parent = cmd.ID_Parent_Other,
                        Type = cmd.Ontology
                    }).FirstOrDefault();

                var commandLineRunReplacePre = model.ReportReplacesToCommandLineRuns.Where(cmd => cmd.ID_Object == configToReportReplace.ID_Other &&
                    cmd.ID_RelationType == FileManagementModule.ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_replace_with_Comand_Line__Run_.ID_RelationType).
                    Select(cmd => new clsOntologyItem
                    {
                        GUID = cmd.ID_Other,
                        Name = cmd.Name_Other,
                        GUID_Parent = cmd.ID_Parent_Other,
                        Type = cmd.Ontology
                    }).FirstOrDefault();
                var testFolder = model.ConfigsToTestPath.Where(testP => testP.ID_Object == config.GUID).Select(testP => testP.Name_Other).FirstOrDefault();

                var reportResult = await reportController.GetReport(report);
                result = reportResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var getFilterItemRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByFilterOItems) { FilterOItems = reportFilter, MessageOutput = messageOutput };
                var filterResult = await reportController.GetFilterItems(getFilterItemRequest);
                result = filterResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var getSortItemRequest = new GetSortItemsRequest(reportSort) { MessageOutput = messageOutput };
                var sortResult = await reportController.GetSortItems(getSortItemRequest);
                result = sortResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var commandLineRunSearch = await commandLineRunController.GetCommandLineRun(commandLineRunSearchPre);
                result = commandLineRunSearch.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var commandLineRunReplace = await commandLineRunController.GetCommandLineRun(commandLineRunReplacePre);
                result = commandLineRunReplace.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var variableFieldMaps = model.ReportReplacesToVariableFields.Where(repRep => repRep.ID_Object == configToReportReplace.ID_Other).Select(repRep => new clsOntologyItem
                {
                    GUID = repRep.ID_Other,
                    Name = repRep.Name_Other,
                    GUID_Parent = repRep.ID_Parent_Other,
                    Type = repRep.Ontology
                }).ToList();

                var getVariablesToFieldsRequest = new GetVariablesToFieldsRequest(variableFieldMaps)
                {
                    MessageOutput = messageOutput
                };
                var variableFieldMapsResult = await reportController.GetVariablesToField(getVariablesToFieldsRequest);

                result = variableFieldMapsResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                messageOutput?.OutputInfo("Have Parameter-data ...");

                var whereCaluse = filterResult.Result.FirstOrDefault()?.Value.Val_String;
                var sortClaus = sortResult.Result.FirstOrDefault()?.Value.Val_String;

                messageOutput?.OutputInfo("Get data ...");
                var reportDateResult = await reportController.LoadData(reportResult.Result.Report, reportResult.Result.ReportFields, whereCaluse, sortClaus);
                result = reportDateResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                messageOutput?.OutputInfo("Have data ...");

                var variablesSearch = commandLineRunSearch.Result.Variables;
                var variablesReplace = commandLineRunReplace.Result.Variables;
                var variableMapsSearch = (from map in variableFieldMapsResult.Result.Where(varMap => variablesSearch.Any(var => var.GUID == varMap.IdVariable))
                                          join reportField in reportResult.Result.ReportFields on map.IdReportField equals reportField.IdReportField
                                          select new { map, reportField }).ToList();

                var variableMapsReplace = (from map in variableFieldMapsResult.Result.Where(varMap => variablesReplace.Any(var => var.GUID == varMap.IdVariable))
                                           join reportField in reportResult.Result.ReportFields on map.IdReportField equals reportField.IdReportField
                                           select new { map, reportField }).ToList();
                var searchPattern = commandLineRunSearch.Result.CodeParsed;
                var replacePattern = commandLineRunReplace.Result.CodeParsed;
                var reportFieldForPath = reportResult.Result.ReportFields.FirstOrDefault(repField => repField.NameReportField == fieldPath.Name);
                if (reportFieldForPath == null)
                {
                    messageOutput?.OutputError("The Path-Field is not found in the Field-list!");
                    return result;
                }

                var backupFolder = model.ConfigsToBackupPath.Where(backup => backup.ID_Object == config.GUID).Select(backup => backup.Name_Other).FirstOrDefault();
                var backupRootFolder = model.ConfigsBackupRootPath.Where(backup => backup.ID_Object == config.GUID).Select(backup => backup.Val_String).FirstOrDefault();

                for (int i = 0; i < reportDateResult.Result.Count; i++)
                {
                    var row = reportDateResult.Result[i];
                    if (!row.ContainsKey(reportFieldForPath.NameCol))
                    {
                        messageOutput?.OutputWarning($"Ix {i} No field found!");
                    }
                    var file = row[reportFieldForPath.NameCol].ToString();
                    var tempFile = file + ".tmp";
                    var fileName = Path.GetFileName(file);
                    var destFilePath = Path.Combine(Path.GetTempPath(), fileName);
                    var replaceFile = false;

                    var search = searchPattern.ToString();
                    foreach (var variableSearchMap in variableMapsSearch)
                    {
                        if (!row.ContainsKey(variableSearchMap.reportField.NameCol))
                        {
                            messageOutput?.OutputWarning($"Ix {i} No field found!");
                            continue;
                        }
                        var value = row[variableSearchMap.reportField.NameCol].ToString();
                        search = search.Replace($"@{variableSearchMap.map.NameVariable}@", value);
                    }

                    var replace = replacePattern.ToString();
                    foreach (var variableReplaceMap in variableMapsReplace)
                    {
                        if (!row.ContainsKey(variableReplaceMap.reportField.NameCol))
                        {
                            messageOutput?.OutputWarning($"Ix {i} No field found!");
                            continue;
                        }
                        var value = row[variableReplaceMap.reportField.NameCol].ToString();
                        replace = replace.Replace($"@{variableReplaceMap.map.NameVariable}@", value);
                    }
                    var logItem = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = file,
                        GUID_Parent = FileManagementModule.ReplaceContent.Config.LocalData.Class_Replace_Log.GUID,
                        Type = Globals.Type_Object
                    };

                    objectsToSave.Add(logItem);

                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(config, logItem, FileManagementModule.ReplaceContent.Config.LocalData.RelationType_contains));

                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Session_Stamp, sessionStamp));
                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Path, file));
                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Test, testItem?.Val_Bool.Value ?? false));

                    try
                    {
                        var regexSearch = new Regex(search);
                        long fileLine = 0;
                        using (var input = File.OpenText(file))
                        using (var output = new StreamWriter(new FileStream(destFilePath, FileMode.Create), System.Text.Encoding.UTF8))
                        {

                            string line;
                            while (null != (line = input.ReadLine()))
                            {
                                if (cancellationToken.IsCancellationRequested)
                                {
                                    result = Globals.LState_Error.Clone();
                                    result.Additional1 = "Stopped by user!";
                                    messageOutput?.OutputWarning(result.Additional1);
                                    return result;
                                }

                                if (regexSearch.Match(line).Success)
                                {
                                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Searched_String, line, orderId: fileLine));
                                    replaceFile = true;
                                    line = regexSearch.Replace(line, replace);
                                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced_String, line, orderId: fileLine));
                                }

                                output.WriteLine(line);
                                fileLine++;
                            }
                        }

                        if (replaceFile)
                        {
                            if (!string.IsNullOrEmpty(backupFolder))
                            {
                                var fileFolder = Path.GetDirectoryName(file);
                                if (!string.IsNullOrEmpty(backupRootFolder))
                                {
                                    fileFolder = fileFolder.Replace(backupRootFolder, backupFolder);
                                }
                                else
                                {
                                    fileFolder = backupFolder;
                                }

                                var backupFile = Path.Combine(fileFolder, fileName);

                                if (!Directory.Exists(fileFolder))
                                {
                                    Directory.CreateDirectory(fileFolder);
                                }
                                File.Copy(file, backupFile, true);
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Backup_Path, backupFile));
                            }

                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Found, true));
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced, true));
                            if ((testItem != null && !testItem.Val_Bool.Value) || testItem == null)
                            {
                                File.Move(file, tempFile);
                                File.Move(destFilePath, file);
                                File.Delete(tempFile);
                            }

                        }
                        else
                        {
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Found, false));
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced, false));
                        }
                        var dateTimeFinished = DateTime.Now;
                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_DateTimestamp, dateTimeFinished));
                    }
                    catch (Exception ex)
                    {
                        messageOutput?.OutputError(ex.Message);
                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Error, ex.Message));
                    }
                }

                if (objectsToSave.Count > saveCount)
                {
                    result = await SaveLog(objectsToSave, attributesToSave, relationsToSave, saveCount, elasticAgent, messageOutput);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        messageOutput?.OutputError(result.Additional1);
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> ReplaceInFile(ElasticAgent elasticAgent,
            clsOntologyItem config,
            ReplaceContentModel model,
            List<clsOntologyItem> objectsToSave,
            List<clsObjectAtt> attributesToSave,
            List<clsObjectRel> relationsToSave,
            clsRelationConfig relationConfig,
            IMessageOutput messageOutput,
            DateTime sessionStamp,
            int saveCount,
            CancellationToken cancellationToken)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();
                var paths = model.ConfigsToPath.Where(pathRel => pathRel.ID_Object == config.GUID).Select(pathRel => pathRel.Name_Other).ToList();
                var regexSearchItem = model.ConfigsRegex.FirstOrDefault(reg => reg.ID_Object == config.GUID);
                var replaceStringItem = model.ConfigsReplaceString.FirstOrDefault(rep => rep.ID_Object == config.GUID);
                var searchString = model.ConfigsSearch.FirstOrDefault(searchItm => searchItm.ID_Object == config.GUID);
                var filterItem = model.ConfigsFilter.FirstOrDefault(filt => filt.ID_Object == config.GUID);
                var recursiveItem = model.ConfigsRecursive.FirstOrDefault(rec => rec.ID_Object == config.GUID);
                var testItem = model.ConfigsTest.FirstOrDefault(test => test.ID_Object == config.GUID);
                var caseSensitiveItem = model.ConfigsCaseSensitive.FirstOrDefault(caseS => caseS.ID_Object == config.GUID);
                var excludes = model.ConfigsToExcludePath.Where(excl => excl.ID_Object == config.GUID).Select(excl => excl.Name_Other).ToList();
                var backupFolder = model.ConfigsToBackupPath.Where(backup => backup.ID_Object == config.GUID).Select(backup => backup.Name_Other).FirstOrDefault();
                var backupRootFolder = model.ConfigsBackupRootPath.Where(backup => backup.ID_Object == config.GUID).Select(backup => backup.Val_String).FirstOrDefault();
                var testFolder = model.ConfigsToTestPath.Where(testP => testP.ID_Object == config.GUID).Select(testP => testP.Name_Other).FirstOrDefault();
                var noLogging = model.ConfigsToNoLogging.FirstOrDefault(noLog => noLog.ID_Object == config.GUID)?.Val_Bit ?? false;

                try
                {
                    if (!string.IsNullOrEmpty(backupFolder) && !Directory.Exists(backupFolder))
                    {
                        Directory.CreateDirectory(backupFolder);
                    }
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = $"Config: {config.Name}, Path: {backupFolder}: {ex.Message}";
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var searchPattern = "*.*";
                var searchOption = SearchOption.TopDirectoryOnly;
                if (recursiveItem != null && recursiveItem.Val_Bool.Value)
                {
                    searchOption = SearchOption.AllDirectories;
                }

                var commonPathResult = GetCommonPath(paths);

                foreach (var path in paths)
                {
                    var files = Directory.GetFiles(path, searchPattern, searchOption).ToList();
                    var regex = new Regex(filterItem.Val_String);
                    files = files.Where(file => regex.Match(file).Success).ToList();
                    var fileRootPath = path;
                    if (commonPathResult.ResultState.GUID == Globals.LState_Success.GUID)
                    {
                        fileRootPath = commonPathResult.Result;
                    }

                    string search = string.Empty;
                    Regex regexSearch = null;
                    if (regexSearchItem != null)
                    {
                        regexSearch = new Regex(regexSearchItem.Val_String);
                    }

                    if (searchString != null)
                    {
                        search = searchString.Val_String;
                    }

                    var replace = "";
                    if (replaceStringItem != null)
                    {
                        replace = replaceStringItem.Val_String;
                    }

                    var caseSensitive = false;
                    if (caseSensitiveItem != null)
                    {
                        caseSensitive = caseSensitiveItem.Val_Bool.Value;
                    }

                    var filesToExclude = (from fl in files
                                          from excl in excludes
                                          where fl.ToLower().StartsWith(excl.ToLower())
                                          select fl).ToList();

                    foreach (var file in (from fl in files
                                          join excl in filesToExclude on fl equals excl into excls
                                          from excl in excls.DefaultIfEmpty()
                                          where excl == null
                                          select fl))
                    {
                        var tempFile = file + ".tmp";
                        var fileName = Path.GetFileName(file);
                        var destFilePath = Path.Combine(Path.GetTempPath(), fileName);
                        var replaceFile = false;
                        var testFilePath = string.Empty;
                        var fileToCheck = file;
                        if (!string.IsNullOrEmpty(testFolder))
                        {
                            if (fileRootPath.EndsWith("\\") && !testFolder.EndsWith("\\"))
                            {
                                testFolder += "\\";
                            }
                            testFilePath = file.Replace(fileRootPath, testFolder);
                            if (!File.Exists(testFilePath))
                            {
                                var pathOfFile = Path.GetDirectoryName(testFilePath);
                                if (!Directory.Exists(pathOfFile))
                                {
                                    Directory.CreateDirectory(pathOfFile);
                                }
                                System.IO.File.Copy(file, testFilePath);
                            }

                            fileToCheck = testFilePath;
                        }

                        var logItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = fileName,
                            GUID_Parent = FileManagementModule.ReplaceContent.Config.LocalData.Class_Replace_Log.GUID,
                            Type = Globals.Type_Object
                        };
                        objectsToSave.Add(logItem);

                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(config, logItem, FileManagementModule.ReplaceContent.Config.LocalData.RelationType_contains));

                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Session_Stamp, sessionStamp));
                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Path, file));
                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Test, testItem?.Val_Bool.Value ?? false));

                        try
                        {
                            long fileLine = 0;
                            using (var input = File.OpenText(fileToCheck))
                            using (var output = new StreamWriter(new FileStream(destFilePath, FileMode.Create), System.Text.Encoding.UTF8))
                            {

                                string line;
                                while (null != (line = input.ReadLine()))
                                {
                                    if (cancellationToken.IsCancellationRequested)
                                    {
                                        result = Globals.LState_Error.Clone();
                                        result.Additional1 = "Stopped by user!";
                                        messageOutput?.OutputWarning(result.Additional1);
                                        return result;
                                    }
                                    if (regexSearch != null)
                                    {
                                        if (regexSearch.Match(line).Success)
                                        {
                                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Searched_String, line, orderId: fileLine));
                                            replaceFile = true;
                                            line = regexSearch.Replace(line, replace);
                                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced_String, line, orderId: fileLine));
                                        }
                                    }
                                    else
                                    {
                                        if (caseSensitive)
                                        {
                                            if (line.Contains(search))
                                            {
                                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Searched_String, line, orderId: fileLine));
                                                replaceFile = true;
                                                line = line.Replace(search, replace);
                                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced_String, line, orderId: fileLine));
                                            }
                                        }
                                        else
                                        {
                                            if (line.ToLower().Contains(search.ToLower()))
                                            {
                                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Searched_String, line, orderId: fileLine));
                                                while (line.ToLower().Contains(search.ToLower()))
                                                {

                                                    replaceFile = true;
                                                    var newLine = "";
                                                    var index = line.ToLower().IndexOf(search.ToLower());
                                                    var length = search.Length;
                                                    if (index > 0)
                                                    {
                                                        newLine = line.Substring(0, index);
                                                    }
                                                    newLine += replace;
                                                    newLine += line.Substring(index + search.Length);
                                                    line = newLine;
                                                }
                                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced_String, line, orderId: fileLine));
                                            }

                                        }

                                    }
                                    output.WriteLine(line);
                                    fileLine++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            result = Globals.LState_Error.Clone();
                            result.Additional1 = ex.Message;
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Error, result.Additional1));
                        }

                        try
                        {
                            if (replaceFile)
                            {
                                if (!string.IsNullOrEmpty(backupFolder) && string.IsNullOrEmpty(testFolder))
                                {
                                    var fileFolder = Path.GetDirectoryName(fileToCheck);
                                    if (!string.IsNullOrEmpty(backupRootFolder))
                                    {
                                        fileFolder = fileFolder.Replace(backupRootFolder, backupFolder);
                                    }
                                    else
                                    {
                                        fileFolder = backupFolder;
                                    }

                                    var backupFile = Path.Combine(fileFolder, fileName);

                                    if (!Directory.Exists(fileFolder))
                                    {
                                        Directory.CreateDirectory(fileFolder);
                                    }
                                    File.Copy(fileToCheck, backupFile, true);
                                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Backup_Path, backupFile));
                                }

                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Found, true));
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced, true));
                                var doTest = (testItem != null && testItem.Val_Bool.Value) || !string.IsNullOrEmpty(testFolder);
                                if (!doTest)
                                {
                                    File.Move(fileToCheck, tempFile);
                                    File.Move(destFilePath, fileToCheck);
                                    File.Delete(tempFile);

                                    if (!string.IsNullOrEmpty(testFolder))
                                    {
                                        attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Test_Path, fileToCheck));
                                    }
                                }

                            }
                            else
                            {
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Found, false));
                                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Replaced, false));
                            }
                            var dateTimeFinished = DateTime.Now;
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_DateTimestamp, dateTimeFinished));
                        }
                        catch (Exception ex)
                        {
                            result = Globals.LState_Error.Clone();
                            result.Additional1 = ex.Message;
                            attributesToSave.Add(relationConfig.Rel_ObjectAttribute(logItem, FileManagementModule.ReplaceContent.Config.LocalData.AttributeType_Error, result.Additional1));
                        }


                        if (objectsToSave.Count > saveCount)
                        {
                            if (!noLogging)
                            {
                                result = await SaveLog(objectsToSave, attributesToSave, relationsToSave, saveCount, elasticAgent, messageOutput);
                            }
                            else
                            {
                                objectsToSave.Clear();
                                attributesToSave.Clear();
                                relationsToSave.Clear();
                            }

                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                messageOutput?.OutputError(result.Additional1);
                                return result;
                            }
                        }
                    }

                    if (objectsToSave.Count > saveCount)
                    {
                        if (!noLogging)
                        {
                            result = await SaveLog(objectsToSave, attributesToSave, relationsToSave, saveCount, elasticAgent, messageOutput);
                        }
                        else
                        {
                            objectsToSave.Clear();
                            attributesToSave.Clear();
                            relationsToSave.Clear();
                        }

                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            messageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                    }
                }
                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> SaveLog(
            List<clsOntologyItem> objectsToSave,
            List<clsObjectAtt> attributesToSave,
            List<clsObjectRel> relationsToSave,
            int saveCount,
            ElasticAgent elasticAgent,
            IMessageOutput messageOutput = null)
        {

            messageOutput?.OutputInfo($"Saved {saveCount} scans");
            var result = await elasticAgent.SaveObjects(objectsToSave);
            if (result.GUID == Globals.LState_Error.GUID)
            {
                result.Additional1 = "Error while saving the Log Entries!";
                return result;
            }
            objectsToSave.Clear();

            if (relationsToSave.Any())
            {
                result = await elasticAgent.SaveRelations(relationsToSave);
            }
            relationsToSave.Clear();

            if (attributesToSave.Any())
            {
                result = await elasticAgent.SaveAttributes(attributesToSave);
            }
            attributesToSave.Clear();

            return result;
        }

        public ResultItem<string> GetCommonPath(List<string> paths)
        {
            var smallestLength = paths.Select(path => path.Length).Min();
            var result = new ResultItem<string>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = ""
            };

            for (int i = 0; i < smallestLength; i++)
            {
                var character = paths[0].Substring(i, 1);
                if (paths.All(path => path.Substring(i, 1) == character))
                {
                    result.Result += character;
                }
                else
                {
                    break;
                }
            }
            try
            {
                if (!Directory.Exists(result.Result))
                {
                    Directory.CreateDirectory(result.Result);
                    Directory.Delete(result.Result);
                }
            }
            catch (Exception ex)
            {
                result.ResultState = Globals.LState_Error.Clone();
                return result;
            }

            return result;
        }

        public FileManagementController(Globals globals) : base(globals)
        {
        }
    }
}
