﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileManagementModule
{
    public class FileSystemNameProvider : INameTransform
    {
        public bool IsReferenceCompatible => true;

        public bool IsAspNetProject { get; set; }

        private Globals globals;

        public bool IsResponsible(string idClass)
        {
            return Config.LocalData.Class_File.GUID == idClass ||
                   Config.LocalData.Class_Folder.GUID == idClass;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
            {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = items
               };
               var fileWorkManager = new MediaStore_Module.FileWorkManager(globals);

               foreach (var item in items)
               {
                   var path = fileWorkManager.GetPathFileSystemObject(item);

                   if (encodeName)
                   {
                       item.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(path)); ;
                   }
                   else
                   {
                       item.Name = path;
                   }
               }

               return result;
            });

            return taskResult;
        }

        public FileSystemNameProvider(Globals globals)
        {
            this.globals = globals;
        }
    }
}
