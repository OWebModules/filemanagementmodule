﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ExportOntologyVersionsRequest
    {
        public string PathToExportTo { get; set; }
        public List<string> NamespacesToExport { get; set; } = new List<string>();

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
    }
}
