﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class OntologyVersion
    {
        public string IdAttributeMajor { get; set; }
        public long Major { get; set; }
        public string IdAttributeMinor { get; set; }
        public long Minor { get; set; }
        public string IdAttributeBuild { get; set; }
        public long Build { get; set; }
        public string IdAttributeRevision { get; set; }
        public long Revision { get; set; }

        public string IdNamespace { get; set; }
        public string Namespace { get; set; }

        public string IdPath { get; set; }
        public string Path { get; set; }

        public string IdOntologyVersion { get; set; }
        public string OntologyVersionName { get; set; }

        public string IdOntology { get; set; }
        public string Ontology { get; set; }

    }
}
