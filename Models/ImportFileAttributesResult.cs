﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ImportFileAttributesResult
    {
        public long FolderCount { get; set; }
        public long FileCount { get; set; }
        public long FileSize { get; set; }

        public long ErrorCount { get; set; }
    }
}
