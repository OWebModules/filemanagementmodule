﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetFilesOfFoldersRequest
    {
        public List<clsOntologyItem> Folders { get; private set; } = new List<clsOntologyItem>();

        public IMessageOutput MessageOutput { get; set; }

        public GetFilesOfFoldersRequest(List<clsOntologyItem> folders)
        {
            Folders = folders;
        }
    }
}
