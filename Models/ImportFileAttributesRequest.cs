﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ImportFileAttributesRequest
    {
        public string IdConfig { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public ImportFileAttributesRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
