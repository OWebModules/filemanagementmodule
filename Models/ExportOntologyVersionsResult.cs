﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ExportOntologyVersionsResult
    {
        public List<OntologyVersion> OntologyVersions { get; set; } = new List<OntologyVersion>();
    }
}
