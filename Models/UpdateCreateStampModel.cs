﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class UpdateCreateStampModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsOntologyItem> Files { get; set; }
        public List<clsObjectAtt> CreateStamps { get; set; }
    }
}
