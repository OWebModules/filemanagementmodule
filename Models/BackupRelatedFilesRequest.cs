﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class BackupRelatedFilesRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; private set; }
        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public BackupRelatedFilesRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }
    }
}
