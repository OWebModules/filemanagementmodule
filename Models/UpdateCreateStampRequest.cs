﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class UpdateCreateStampRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public UpdateCreateStampRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
