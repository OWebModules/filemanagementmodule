﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class UpdateAssembliesRequest
    {
        public string Path { get; set; }
        public bool UseDbPaths { get; set; }

        public Regex ExcludeRegex { get; set; }
        public Regex IncludeRegex { get; set; }

        public bool Test { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
    }
}
