﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ReplaceContentResult
    {
        public List<ReplaceContentResultItem> ResultItem { get; set; } = new List<ReplaceContentResultItem>();
    }

    public class ReplaceContentResultItem
    {
        public clsOntologyItem ResultState { get; set; }
        public clsOntologyItem LogItem { get; set; }
    }
}
