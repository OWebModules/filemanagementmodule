﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class RelateExternalFilesModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> ConfigToFolders { get; set; }
        public clsObjectAtt SubItems { get; set; }
        public clsObjectAtt CreateBlobIfNewFile { get; set; }
    }
}
