﻿using FileManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateCalculateHashRequest(CalculateHashForBlobFilesRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is no GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateUpdateCreateSatmpRequest(UpdateCreateStampRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is no GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateUpdateCreateStampModel(UpdateCreateStampModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( propertyName ) || propertyName == nameof(UpdateCreateStampModel.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Config found!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateReplaceContentRequest(ReplaceContentRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateReplaceContentModel(ReplaceContentModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( propertyName) || propertyName == nameof(ReplaceContentModel.BaseConfig) )
            {
                if (model.BaseConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Base-Config provided!";
                    return result;
                }

                if (model.BaseConfig.GUID_Parent != ReplaceContent.Config.LocalData.Class_Replace_Content_in_Files.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Base-Config is of wrong class!";
                    return result;
                }

            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsCaseSensitive))
            {
                if (model.ConfigsCaseSensitive.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Case Sensitive Attributes as Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsFilter))
            {
                if (model.ConfigsFilter.Count != model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Filters as Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsRecursive))
            {
                if (model.ConfigsRecursive.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Recursive Attribute as Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsRegex))
            {
                if (model.ConfigsRegex.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Regex-Replace as Configs!";
                    return result;
                }

                if (model.ConfigsReplaceString.Count > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You can only define Regex-Replace or normal Replace!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsReplaceString))
            {
                if (model.ConfigsReplaceString.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Replace Strings as Configs!";
                    return result;
                }

                if (model.ConfigsRegex.Count > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You can only define Regex-Replace or normal Replace!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsSearch))
            {
                if (model.ConfigsSearch.Count != model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need exact the number of search strings as configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsTest))
            {
                if (model.ConfigsTest.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have more Test Attributes as Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsToPath))
            {
                var noConfigPaths = (from config in model.Configs
                                     join path in model.ConfigsToPath on config.GUID equals path.ID_Object into paths
                                     from path in paths.DefaultIfEmpty()
                                     where path == null
                                     select config).ToList();

                if (noConfigPaths.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Each config needs at least one path!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsToBackupPath))
            {
                if (model.ConfigsToBackupPath.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must not have more backup-paths then configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsBackupRootPath))
            {
                if (model.ConfigsBackupRootPath.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must not have more backup rootpath configs then configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ReportReplacesToCommandLineRuns))
            {
                var checkRelationsCount1 = (from configToRepReplace in model.ConfigsToReportReplace
                                      join replaceToSearchCommandLine in model.ReportReplacesToCommandLineRuns.Where(rel => rel.ID_RelationType == ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_replace_with_Comand_Line__Run_.ID_RelationType) on
                                        configToRepReplace.ID_Other equals replaceToSearchCommandLine.ID_Object
                                      select configToRepReplace).Count();
                var checkRelationsCount2 = (from configToRepReplace in model.ConfigsToReportReplace
                                            join replaceToSearchCommandLine in model.ReportReplacesToCommandLineRuns.Where(rel => rel.ID_RelationType == ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_search_Comand_Line__Run_.ID_RelationType) on
                                              configToRepReplace.ID_Other equals replaceToSearchCommandLine.ID_Object
                                            select configToRepReplace).Count();

                if (model.ConfigsToReportReplace.Count != checkRelationsCount1 || model.ConfigsToReportReplace.Count != checkRelationsCount2)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must have a search and replace-command line run per Report-Replace!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ReportReplacesToReportFieldFilePaths))
            {
                if (model.ConfigsToReportReplace.Count != model.ReportReplacesToReportFieldFilePaths.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must have one Report-Field as File-Path!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ReportReplacesToReportFilters))
            {
                if (model.ConfigsToReportReplace.Count > model.ReportReplacesToReportFilters.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have provided more filters as Report-Replaces!";
                    return result;
                }
            }


            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ReportReplacesToReports))
            {
                if (model.ConfigsToReportReplace.Count != model.ReportReplacesToReports.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need one Report per Report-Replace!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ReportReplacesToReportSorts))
            {
                if (model.ConfigsToReportReplace.Count > model.ReportReplacesToReportSorts.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need one Report per Report-Sorts!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.VariableFieldsToReportFields))
            {
                if (model.ConfigsToReportReplace.Count > model.VariableFieldsToReportFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need at least one Variable to Field per Report Replace!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReplaceContentModel.ConfigsToNoLogging))
            {
                if (model.Configs.Count < model.ConfigsToNoLogging.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Logging must be defined max one time for each config!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateReferenceExternalFilesRequest(RelateExternalFilesRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetReferenceExternalFilesModel(RelateExternalFilesModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The count of possible Configs is not 1!";
                    return result;
                }
                model.Config = dbReader.Objects1.First();
                if (model.Config.GUID_Parent != RelateExternalFiles.Config.LocalData.Class_Relate_external_Files.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The found Object is not of class {RelateExternalFiles.Config.LocalData.Class_Relate_external_Files.Name}";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.SubItems))
            {
                if (dbReader.ObjAtts.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Only one Attribute for Subitems is allowed!";
                    return result;
                }
                model.SubItems = dbReader.ObjAtts.FirstOrDefault();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigToFolders))
            {
                if (!dbReader.ObjectRels.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must relate at least one path to search for files!";
                    return result;
                }

                model.ConfigToFolders = dbReader.ObjectRels;
                var invalidObjects = model.ConfigToFolders.Where(rel => rel.ID_Parent_Other != Config.LocalData.Class_Folder.GUID).ToList();
                if (invalidObjects.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There are {invalidObjects.Count} of {model.ConfigToFolders.Count} related objects, which are no folders!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.CreateBlobIfNewFile))
            {
                if (dbReader.ObjAtts.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must relate max one attribute for the flag to create a blob if new file!";
                    return result;
                }

                model.CreateBlobIfNewFile = dbReader.ObjAtts.FirstOrDefault();
            }

            return result;
        }

    }
}
