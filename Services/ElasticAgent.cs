﻿using FileManagementModule.Models;
using FileManagementModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<GetBackupRelatedFilesModelResult>> GetBackupRelatedFilesModel(BackupRelatedFilesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetBackupRelatedFilesModelResult>>(() =>
           {
               var result = new ResultItem<GetBackupRelatedFilesModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetBackupRelatedFilesModelResult()
               };

               if (string.IsNullOrEmpty(request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The config-id is not valid!";
                   return result;
               }

               if (!globals.is_GUID(request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The config-id is not a valid GUID!";
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the rott-config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.SingleOrDefault();

               if (result.Result.RootConfig == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Root-config found!";
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = BackupRelated.Config.LocalData.ClassRel_Backup_Related_Files_contains_Backup_Related_Files.ID_RelationType,
                       ID_Parent_Other = BackupRelated.Config.LocalData.ClassRel_Backup_Related_Files_contains_Backup_Related_Files.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the subconfigs!";
                   return result;
               }

               result.Result.ConfigItems = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.ConfigItems.Any())
               {
                   result.Result.ConfigItems.Add(result.Result.RootConfig);
               }

               var searchRelationLevels = result.Result.ConfigItems.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = BackupRelated.Config.LocalData.AttributeType_Relation_Level.GUID
               }).ToList();

               var dbReaderRelationLevels = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelationLevels.GetDataObjectAtt(searchRelationLevels);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation-levels!";
                   return result;
               }

               result.Result.RelationLevels = dbReaderRelationLevels.ObjAtts;

               if (!result.Result.RelationLevels.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must define relation-levels!";
                   return result;
               }

               var searchPaths = result.Result.ConfigItems.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = BackupRelated.Config.LocalData.ClassRel_Backup_Related_Files_belonging_Destination_Path.ID_RelationType,
                   ID_Parent_Other = BackupRelated.Config.LocalData.ClassRel_Backup_Related_Files_belonging_Destination_Path.ID_Class_Right
               }).ToList();

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the paths!";
                   return result;
               }

               result.Result.Paths = dbReaderPaths.ObjectRels;

               var searchObjects = result.Result.ConfigItems.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = BackupRelated.Config.LocalData.ClassRel_Backup_Related_Files_belonging_Object.ID_RelationType
               }).ToList();

               var dbReaderObjects = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the objects!";
                   return result;
               }

               result.Result.Objects = dbReaderObjects.ObjectRels;

               if (!result.Result.Objects.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No objects found!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetFilesOfFolders(List<clsOntologyItem> folders)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
              {
                  var result = new ResultItem<List<clsObjectRel>>
                  {
                      ResultState = globals.LState_Success.Clone(),
                      Result = new List<clsObjectRel>()
                  };

                  var searchFiles = folders.Select(folder => new clsObjectRel
                  {
                      ID_Other = folder.GUID,
                      ID_RelationType = Config.LocalData.ClassRel_File_is_subordinated_Folder.ID_RelationType,
                      ID_Parent_Object = Config.LocalData.ClassRel_File_is_subordinated_Folder.ID_Class_Left
                  }).ToList();

                  var dbReaderFilesOfFolders = new OntologyModDBConnector(globals);

                  if (searchFiles.Any())
                  {
                      result.ResultState = dbReaderFilesOfFolders.GetDataObjectRel(searchFiles);

                      if (result.ResultState.GUID == globals.LState_Error.GUID)
                      {
                          result.ResultState.Additional1 = "Error while getting the files!";
                          return result;
                      }
                  }

                  result.Result = dbReaderFilesOfFolders.ObjectRels;

                  return result;
              });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetHasAttributesOfFiles(List<clsOntologyItem> fileItems)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<clsObjectAtt>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectAtt>()
                };

                var searchHashValues = fileItems.Select(fileItem => new clsObjectAtt
                {
                    ID_Object = fileItem.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Hash.GUID
                }).ToList();

                var dbReaderHasValues = new OntologyModDBConnector(globals);

                if (searchHashValues.Any())
                {
                    result.ResultState = dbReaderHasValues.GetDataObjectAtt(searchHashValues);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the hash-values!";
                        return result;
                    }

                    result.Result = dbReaderHasValues.ObjAtts;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetRelations(List<clsObjectRel> search)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               if (!search.Any()) return result;

               var dbReader = new OntologyModDBConnector(globals);

               result.ResultState = dbReader.GetDataObjectRel(search);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error wwhile getting the relations!";
                   return result;
               }

               result.Result = dbReader.ObjectRels;

               return result;
           });

            return taskResult;

        }

        public async Task<ResultItem<GetUpdateOntologyModelResult>> GetUpdateOntologyModel()
        {
            var taskResult = await Task.Run<ResultItem<GetUpdateOntologyModelResult>>(() =>
           {
               var result = new ResultItem<GetUpdateOntologyModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetUpdateOntologyModelResult()
               };

               var searchOntologyVersions = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_Ontology_Update.GUID
                   }
               };

               var dbReaderOntologyVersions = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderOntologyVersions.GetDataObjects(searchOntologyVersions);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Ontology-Versions";
                   return result;
               }

               result.Result.OntologieVersions = dbReaderOntologyVersions.Objects1;

               var searchOntologyAttributes = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_AttributeType = Config.LocalData.AttributeType_Major.GUID
                   },
                   new clsObjectAtt
                   {
                       ID_AttributeType = Config.LocalData.AttributeType_Minor.GUID
                   },
                   new clsObjectAtt
                   {
                       ID_AttributeType = Config.LocalData.AttributeType_Build.GUID
                   },
                   new clsObjectAtt
                   {
                       ID_AttributeType = Config.LocalData.AttributeType_Revision.GUID
                   }
               };

               var dbReaderOntologyVersionAttributes = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderOntologyVersionAttributes.GetDataObjectAtt(searchOntologyAttributes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the attributes of Ontology-Versions!";
                   return result;
               }

               result.Result.OntologyVersionAttribs = dbReaderOntologyVersionAttributes.ObjAtts;

               var searchOntologyVersionNamespaces = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_Class_Left,
                       ID_RelationType = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_Class_Right
                   }
               };

               var dbReaderNamespaces = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderNamespaces.GetDataObjectRel(searchOntologyVersionNamespaces);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the namespaces!";
                   return result;
               }

               result.Result.OntologyVersionsToNameSpaces = dbReaderNamespaces.ObjectRels;

               var searchPaths = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_Class_Left,
                       ID_RelationType = Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_Class_Right
                   }
               };

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the paths!";
                   return result;
               }

               result.Result.OntologyVersionsToPaths = dbReaderPaths.ObjectRels;

               var searchOntologies = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_Class_Left,
                       ID_RelationType = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_Class_Right
                   }
               };

               var dbReaderOntologies = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderOntologies.GetDataObjectRel(searchOntologies);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the ontologies!";
                   return result;
               }

               result.Result.OntologyVersionsToOntologies = dbReaderOntologies.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveNewFileItem(clsOntologyItem fileItem, clsOntologyItem folderItem)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjects(new List<clsOntologyItem>
                {
                    fileItem
                });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the file!";
                    return result;
                }

                var relationConfig = new clsRelationConfig(globals);

                var relToSave = relationConfig.Rel_ObjectRelation(fileItem, folderItem, Config.LocalData.RelationType_is_subordinated);

                result = dbWriter.SaveObjRel(new List<clsObjectRel> { relToSave });
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving of relation between file and folder!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveNewSubFolderItem(clsOntologyItem parentFolder, clsOntologyItem subFolder)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjects(new List<clsOntologyItem>
                {
                    subFolder
                });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the subfolder!";
                    return result;
                }

                var relationConfig = new clsRelationConfig(globals);

                var relToSave = relationConfig.Rel_ObjectRelation(subFolder, parentFolder, Config.LocalData.RelationType_is_subordinated);

                result = dbWriter.SaveObjRel(new List<clsObjectRel> { relToSave });
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving of relation between subfolder and parentfolder!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> RelateFileToFolder(clsOntologyItem fileItem, clsOntologyItem folderItem)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                var relationConfig = new clsRelationConfig(globals);

                var relToSave = relationConfig.Rel_ObjectRelation(fileItem, folderItem, Config.LocalData.RelationType_is_subordinated);

                result = dbWriter.SaveObjRel(new List<clsObjectRel> { relToSave });
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving of relation between file and folder!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> RelateSubFolder(clsOntologyItem parentFolder, clsOntologyItem subFolder)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                var relationConfig = new clsRelationConfig(globals);

                var relToSave = relationConfig.Rel_ObjectRelation(subFolder, parentFolder, Config.LocalData.RelationType_is_subordinated);

                result = dbWriter.SaveObjRel(new List<clsObjectRel> { relToSave });
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving of relation between file and folder!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveOntologyVersion(OntologyVersion version)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var transaction = new clsTransaction(globals);
                var relationConfig = new clsRelationConfig(globals);
                var result = globals.LState_Success.Clone();

                var ontologyUpdate = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = version.Namespace,
                    GUID_Parent = Config.LocalData.Class_Ontology_Update.GUID,
                    Type = globals.Type_Object
                };

                result = transaction.do_Transaction(ontologyUpdate);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Ontology-Version!";
                    return result;
                }

                var searchNameSapce = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        Name = version.Namespace,
                        GUID_Parent = Config.LocalData.Class_Namespace.GUID
                    }
                };

                var dbReaderNameSpace = new OntologyModDBConnector(globals);

                result = dbReaderNameSpace.GetDataObjects(searchNameSapce);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while searching the namespace!";
                    transaction.rollback();
                    return result;
                }

                var namespaceItem = dbReaderNameSpace.Objects1.FirstOrDefault();

                if (namespaceItem == null)
                {
                    namespaceItem = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = version.Namespace,
                        GUID_Parent = Config.LocalData.Class_Namespace.GUID,
                        Type = globals.Type_Object
                    };

                    result = transaction.do_Transaction(namespaceItem);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving new namespace!";
                        transaction.rollback();
                        return result;
                    }
                }

                var rel = relationConfig.Rel_ObjectRelation(ontologyUpdate, namespaceItem, Config.LocalData.RelationType_belongs_to);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the namespace of ontology-version!";
                    transaction.rollback();
                    return result;
                }

                var searchPath = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        Name = version.Path,
                        GUID_Parent = Config.LocalData.Class_Path.GUID
                    }
                };

                var dbReaderPath = new OntologyModDBConnector(globals);

                result = dbReaderPath.GetDataObjects(searchPath);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while searching for exising path";
                    transaction.rollback();
                    return result;
                }

                var path = dbReaderPath.Objects1.FirstOrDefault();

                if (path == null)
                {
                    path = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = version.Path,
                        GUID_Parent = Config.LocalData.Class_Path.GUID,
                        Type = globals.Type_Object
                    };

                    result = transaction.do_Transaction(path);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving path!";
                        transaction.rollback();
                        return result;
                    }
                }

                rel = relationConfig.Rel_ObjectRelation(ontologyUpdate, path, Config.LocalData.RelationType_belonging_Source);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving relation between ontology-version and path!";
                    transaction.rollback();
                    return result;
                }

                var attr = relationConfig.Rel_ObjectAttribute(ontologyUpdate, Config.LocalData.AttributeType_Major, version.Major);

                result = transaction.do_Transaction(attr);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving major attribute!";
                    transaction.rollback();
                    return result;
                }

                version.IdAttributeMajor = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;

                attr = relationConfig.Rel_ObjectAttribute(ontologyUpdate, Config.LocalData.AttributeType_Minor, version.Minor);

                result = transaction.do_Transaction(attr);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving minor attribute!";
                    transaction.rollback();
                    return result;
                }

                version.IdAttributeMinor = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;

                attr = relationConfig.Rel_ObjectAttribute(ontologyUpdate, Config.LocalData.AttributeType_Build, version.Build);

                result = transaction.do_Transaction(attr);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving build attribute!";
                    transaction.rollback();
                    return result;
                }

                version.IdAttributeBuild = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;

                attr = relationConfig.Rel_ObjectAttribute(ontologyUpdate, Config.LocalData.AttributeType_Revision, version.Revision);

                result = transaction.do_Transaction(attr);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving revision attribute!";
                    transaction.rollback();
                    return result;
                }

                var searchOntology = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = version.IdOntology,
                        GUID_Parent = globals.Class_Ontologies.GUID
                    }
                };

                var dbReaderOntologies = new OntologyModDBConnector(globals);

                result = dbReaderOntologies.GetDataObjects(searchOntology);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while searching the ontology!";
                    return result;
                }

                var ontology = dbReaderOntologies.Objects1.FirstOrDefault();

                if (ontology == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Ontology not found!";
                    transaction.rollback();
                    return result;
                }

                version.IdAttributeRevision= transaction.OItem_Last.OItemObjectAtt.ID_Attribute;

                rel = relationConfig.Rel_ObjectRelation(ontologyUpdate, ontology, Config.LocalData.RelationType_belongs_to);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the relation between the ontology-version and the ontology!";
                    transaction.rollback();
                    return result;
                }

                version.IdOntologyVersion = ontologyUpdate.GUID;
                version.OntologyVersionName = ontologyUpdate.Name;
                version.IdPath = path.GUID;
                version.Path = path.Name;
                version.IdNamespace = namespaceItem.GUID;
                version.Namespace = namespaceItem.Name;
                version.IdOntology = ontology.GUID;
                version.Ontology = ontology.Name;

                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<GetFileversionsModelResult>> GetFileversionsModel(GetFileVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetFileversionsModelResult>>(() =>
            {
                var result = new ResultItem<GetFileversionsModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetFileversionsModelResult()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = FileVersions.Config.LocalData.Class_FileVersion.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Fileversions-config!";
                    return result;
                }

                result.Result.FileVersionsConfig = dbReaderConfig.Objects1.FirstOrDefault();

                if (result.Result.FileVersionsConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Fileversions-config found!";
                    return result;
                }

                var searchTextParser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.FileVersionsConfig.GUID,
                        ID_RelationType = FileVersions.Config.LocalData.ClassRel_FileVersion_ParseSource_Textparser.ID_RelationType,
                        ID_Parent_Other = FileVersions.Config.LocalData.ClassRel_FileVersion_ParseSource_Textparser.ID_Class_Right
                    }
                };

                var dbReaderTextParser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Textparser!";
                    return result;
                }

                result.Result.TextParser = dbReaderTextParser.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.TextParser == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Textparser found!";
                    return result;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<clsOntologyItem> objects, bool compareName = true)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                if (!objects.Any())
                {
                    return result;
                }

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjects(objects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the fileversions!";
                    return result;
                }

                if (compareName)
                {
                    (from objectItem in objects
                     join objectItemDb in dbConnector.Objects1 on objectItem.Name equals objectItemDb.Name into fileVersionsDb
                     from objectItemDb in fileVersionsDb.DefaultIfEmpty()
                     select new { objectItem, objectItemDb }).ToList().ForEach(fileVersion =>
                     {
                         if (fileVersion.objectItemDb == null)
                         {
                             if (compareName)
                             {
                                 fileVersion.objectItem.GUID = globals.NewGUID;
                             }
                             fileVersion.objectItem.New_Item = true;
                         }
                         else
                         {
                             if (string.IsNullOrEmpty(fileVersion.objectItem.GUID))
                             {
                                 fileVersion.objectItem.GUID = fileVersion.objectItemDb.GUID;
                             }
                             fileVersion.objectItem.New_Item = false;
                         }
                     });
                }
                else
                {
                    (from objectItem in objects
                     join objectItemDb in dbConnector.Objects1 on objectItem.GUID equals objectItemDb.GUID into fileVersionsDb
                     from objectItemDb in fileVersionsDb.DefaultIfEmpty()
                     select new { objectItem, objectItemDb }).ToList().ForEach(fileVersion =>
                     {
                         if (fileVersion.objectItemDb == null)
                         {
                             
                             fileVersion.objectItem.New_Item = true;
                         }
                         else
                         {
                             
                             fileVersion.objectItem.New_Item = false;
                         }
                     });

                }
               


                var objectsToSave = objects.Where(fileVersion => fileVersion.New_Item.Value).ToList();

                if (objectsToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjects(objectsToSave);
                }

                result.Result = objects;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> CheckRelations(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (!relations.Any())
                {
                    return result;
                }

                var relationsToCheck = relations.GroupBy(rel => new { rel.ID_Object, rel.ID_RelationType, rel.ID_Other }).Select(rel => new clsObjectRel
                {
                    ID_Object = rel.Key.ID_Object,
                    ID_RelationType = rel.Key.ID_RelationType,
                    ID_Other = rel.Key.ID_Other
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjectRel(relationsToCheck, doIds: true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations!";
                    return result;
                }

                var relationsToSave = (from rel in relations
                                       join relDb in dbConnector.ObjectRelsId on new { rel.ID_Object, rel.ID_RelationType, rel.ID_Other } equals new { relDb.ID_Object, relDb.ID_RelationType, relDb.ID_Other } into relDbs
                                       from relDb in relDbs.DefaultIfEmpty()
                                       where relDb == null
                                       select rel).ToList();

                if (relationsToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjRel(relationsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relations!";
                    }
                }
                result.Result = relations;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> CheckRelations(List<clsObjectAtt> attRelations)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
            {
                var result = new ResultItem<List<clsObjectAtt>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectAtt>()
                };

                if (!attRelations.Any())
                {
                    return result;
                }

                var relationsToCheck = attRelations.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Object,
                    ID_AttributeType = rel.ID_AttributeType
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjectAtt(relationsToCheck, doIds:true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the attributes!";
                    return result;
                }

                var relationsToSave = (from rel in attRelations
                                       join relDb in dbConnector.ObjAttsId on new { rel.ID_Object, rel.ID_AttributeType } equals new { relDb.ID_Object, relDb.ID_AttributeType } into relDbs
                                       from relDb in relDbs.DefaultIfEmpty()
                                       where relDb == null
                                       select rel).ToList();

                if (relationsToSave.Any())
                {
                    result.ResultState = dbConnector.SaveObjAtt(relationsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the attributes!";
                    }
                }
                result.Result = attRelations;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetFileAttributesModel>> GetFileAttributesModel(ImportFileAttributesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetFileAttributesModel>>(() =>
           {
               var result = new ResultItem<GetFileAttributesModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetFileAttributesModel()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = FileAttributes.Config.LocalData.Class_Import_File_Attributes.GUID
                   }
               };

               var dbReaderConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigs.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Configs!";
                   return result;
               }

               result.Result.Config = dbReaderConfigs.Objects1.FirstOrDefault();

               if (result.Result.Config == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No config found!";
                   return result;
               }

               var searchFileAttributes = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_uses_File_Attributes.ID_RelationType,
                       ID_Parent_Other = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_uses_File_Attributes.ID_Class_Right
                   }
               };

               var dbReaderFileAttributes = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderFileAttributes.GetDataObjectRel(searchFileAttributes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the File-Attributes!";
                   return result;
               }

               result.Result.ConfigsToFileAttributes = dbReaderFileAttributes.ObjectRels;

               if (!result.Result.ConfigsToFileAttributes.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must provide at least one file-attribute!";
                   return result;
               }

               var searchPath = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_belonging_Source_Path.ID_RelationType,
                       ID_Parent_Other = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_belonging_Source_Path.ID_Class_Right
                   }
               };

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchPath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the paths!";
                   return result;
               }

               result.Result.Path = dbReaderPaths.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.Path == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must provide a path!";
                   return result;
               }

               var searchTextParser = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_belonging_Destination_Textparser.ID_RelationType,
                       ID_Parent_Other = FileAttributes.Config.LocalData.ClassRel_Import_File_Attributes_belonging_Destination_Textparser.ID_Class_Right
                   }
               };

               var dbReaderTextParser = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Textparsers!";
                   return result;
               }

               result.Result.TextParser = dbReaderTextParser.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               if (result.Result.TextParser == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must provide a textparser!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetModelCalculateHashForBlobFilesResult>> GetModelCalculateHashForBlobFiles(CalculateHashForBlobFilesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelCalculateHashForBlobFilesResult>> (() =>
            {
                var result = new ResultItem<GetModelCalculateHashForBlobFilesResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelCalculateHashForBlobFilesResult()
                };

                result.ResultState = ValidationController.ValidateCalculateHashRequest(request, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the config!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.SingleOrDefault();

                if (result.Result.Config == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Config cannot be found!";
                    return result;
                }

                if (result.Result.Config.GUID_Parent != CalculateHash.Config.LocalData.Class_Calculate_Hash.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The config is no Calculate-Hash config!";
                    return result;
                }

                var searchRecalc = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_AttributeType = CalculateHash.Config.LocalData.ClassAtt_Calculate_Hash_Recalc.ID_AttributeType
                    }
                };

                var dbReaderRecalc = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRecalc.GetDataObjectAtt(searchRecalc);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Recalc attribute!";
                    return result;
                }

                result.Result.Recalculate = dbReaderRecalc.ObjAtts.OrderBy(att => att.OrderID).FirstOrDefault();

                var searchFilesFilter = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = CalculateHash.Config.LocalData.ClassRel_Calculate_Hash_uses_File.ID_RelationType,
                        ID_Parent_Other = CalculateHash.Config.LocalData.ClassRel_Calculate_Hash_uses_File.ID_Class_Right
                    }
                };

                var dbReaderFilesFilter = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFilesFilter.GetDataObjectRel(searchFilesFilter);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the files-filter!";
                    return result;
                }

                result.Result.FilesFilter = dbReaderFilesFilter.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                var searchFolderFilter = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = CalculateHash.Config.LocalData.ClassRel_Calculate_Hash_uses_Folder.ID_RelationType,
                        ID_Parent_Other = CalculateHash.Config.LocalData.ClassRel_Calculate_Hash_uses_Folder.ID_Class_Right
                    }
                };

                var dbReaderFolderFilter = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFolderFilter.GetDataObjectRel(searchFolderFilter);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the folder-filter!";
                    return result;
                }

                result.Result.FolderFilter = dbReaderFolderFilter.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<GetFilesFilterForHashingResult>> GetFilesFilterForHashing(GetModelCalculateHashForBlobFilesResult configModel)
        {
            var taskResult = await Task.Run<ResultItem<GetFilesFilterForHashingResult>>(() =>
           {
               var result = new ResultItem<GetFilesFilterForHashingResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetFilesFilterForHashingResult
                   {
                       Recalc = configModel.Recalculate?.Val_Bit.Value ?? false
                   }
               };

               var folderFilter = configModel.FolderFilter.Any();
               if (folderFilter)
               {
                   var searchFilesOfFolders = configModel.FolderFilter.Select(folder => new clsObjectRel
                   {
                       ID_Other = folder.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_File_is_subordinated_Folder.ID_RelationType,
                       ID_Parent_Object = Config.LocalData.ClassRel_File_is_subordinated_Folder.ID_Class_Left
                   }).ToList();

                   var dbReaderGetFilesOfFolders = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderGetFilesOfFolders.GetDataObjectRel(searchFilesOfFolders);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the files of folders!";
                       return result;
                   }

                   result.Result.FilesFilter = dbReaderGetFilesOfFolders.ObjectRels.Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Object,
                       Name = rel.Name_Object,
                       GUID_Parent = rel.ID_Parent_Object,
                       Type = globals.Type_Object
                   }).ToList();
               }

               var filesFilter = configModel.FilesFilter.Any();
               if (filesFilter)
               {
                   if (folderFilter)
                   {
                       result.Result.FilesFilter = (from fileFromFolder in result.Result.FilesFilter
                                                    join fileFilter in configModel.FilesFilter on fileFromFolder.GUID equals fileFilter.GUID
                                                    select fileFromFolder).ToList();
                   }
                   else
                   {
                       var dbReaderFilesFilter = new OntologyModDBConnector(globals);

                       result.ResultState = dbReaderFilesFilter.GetDataObjects(configModel.FilesFilter);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the files!";
                           return result;
                       }

                       result.Result.FilesFilter = dbReaderFilesFilter.Objects1;
                   }
               }

               result.Result.UseFilter = folderFilter || filesFilter;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetBlobFilesForHashing(GetFilesFilterForHashingResult filesFilter, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
           {
               var result = new ResultItem<List<clsObjectAtt>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectAtt>()
               };

               var searchBlobs = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_AttributeType = Config.LocalData.AttributeType_Blob.GUID
                   }
               };

               var dbReaderBlobs = new OntologyModDBConnector(globals);

               messageOutput?.OutputInfo("Get blob-files...");
               result.ResultState = dbReaderBlobs.GetDataObjectAtt(searchBlobs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the blob-attributes!";
                   return result;
               }
               messageOutput?.OutputInfo($"Get blob-files: {dbReaderBlobs.ObjAtts.Count}");

               var hashes = new List<clsObjectAtt>();
               if (!filesFilter.Recalc)
               {
                   var searchHash = new List<clsObjectAtt>
                   {
                       new clsObjectAtt
                       {
                           ID_AttributeType = Config.LocalData.AttributeType_Hash.GUID
                       }
                   };

                   var dbReaderHash = new OntologyModDBConnector(globals);

                   messageOutput?.OutputInfo("Get hashes...");
                   result.ResultState = dbReaderHash.GetDataObjectAtt(searchHash);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the file-hashes!";
                       return result;
                   }

                   hashes = dbReaderHash.ObjAtts;
                   messageOutput?.OutputInfo($"Have hashes: {hashes.Count}");
               }

               messageOutput?.OutputInfo($"Get files...");
               if (filesFilter.UseFilter)
               {
                   if (!filesFilter.Recalc)
                   {
                       result.Result = (from file in filesFilter.FilesFilter
                                        join blob in dbReaderBlobs.ObjAtts on file.GUID equals blob.ID_Object
                                        join hash in hashes on blob.ID_Object equals hash.ID_Object into hashes1
                                        from hash in hashes1.DefaultIfEmpty()
                                        where hash == null
                                        select blob).ToList();
                   }
                   else
                   {
                       result.Result = (from file in filesFilter.FilesFilter
                                        join blob in dbReaderBlobs.ObjAtts on file.GUID equals blob.ID_Object
                                        select blob).ToList();
                   }
                   
               }
               else
               {
                   if (!filesFilter.Recalc)
                   {
                       result.Result = (from blobFile in dbReaderBlobs.ObjAtts
                                        join hash in hashes on blobFile.ID_Object equals hash.ID_Object into hashes1
                                        from hash in hashes1.DefaultIfEmpty()
                                        where hash == null
                                        select blobFile).ToList();
                   }
                   else
                   {
                       result.Result = dbReaderBlobs.ObjAtts;
                   }
                   
               }

               messageOutput?.OutputInfo($"Have files: {result.Result.Count}");

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<UpdateCreateStampModel>> GetUpdateCreateStampModel(UpdateCreateStampRequest request)
        {
            var taskResult = await Task.Run<ResultItem<UpdateCreateStampModel>>(() =>
           {
               var result = new ResultItem<UpdateCreateStampModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new UpdateCreateStampModel()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = UpdateCreateStamp.Config.LocalData.Class_Update_Createstamp.GUID
                   }
               };

               var dbReader = new OntologyModDBConnector(globals);

               result.ResultState = dbReader.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the config!";
                   return result;
               }

               result.Result.Config = dbReader.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateUpdateCreateStampModel(result.Result, globals, nameof(UpdateCreateStampModel.Config));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchFiles = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = UpdateCreateStamp.Config.LocalData.ClassRel_Update_Createstamp_contains_File.ID_RelationType,
                       ID_Parent_Other = UpdateCreateStamp.Config.LocalData.ClassRel_Update_Createstamp_contains_File.ID_Class_Right
                   }
               };

               var dbReaderFilesOfConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderFilesOfConfig.GetDataObjectRel(searchFiles);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the files!";
                   return result;
               }

               result.Result.Files = dbReaderFilesOfConfig.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Files.Any())
               {
                   var searchAllFiles = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID_Parent = Config.LocalData.Class_File.GUID
                       }
                   };

                   result.ResultState = dbReaderFilesOfConfig.GetDataObjects(searchAllFiles);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting all files!";
                       return result;
                   }

                   result.Result.Files = dbReaderFilesOfConfig.Objects1;
               }

               var searchAttributes = result.Result.Files.Select(file => new clsObjectAtt
               {
                   ID_Object = file.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Blob.GUID
               }).ToList();

               searchAttributes.AddRange(result.Result.Files.Select(file => new clsObjectAtt
               {
                   ID_Object = file.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID
               }));

               var dbReaderAttributes = new OntologyModDBConnector(globals);

               if (searchAttributes.Any())
               {
                   result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the file-Attributes!";
                       return result;
                   }
               }

               
               (from file in result.Result.Files
                join blobAttrib in dbReaderAttributes.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == Config.LocalData.AttributeType_Blob.GUID) on file.GUID equals blobAttrib.ID_Object
                select file).ToList().ForEach(file =>
                {
                    file.Mark = true;
                });

               result.Result.CreateStamps = dbReaderAttributes.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == Config.LocalData.AttributeType_Datetimestamp__Create_.GUID).ToList();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<ReplaceContentModel>> GetReplaceContentModel(ReplaceContentRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ReplaceContentModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ReplaceContentModel()
                };

                var searchBaseConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderBaseConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderBaseConfig.GetDataObjects(searchBaseConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Base-Config!";
                    return result;
                }

                result.Result.BaseConfig = dbReaderBaseConfig.Objects1.FirstOrDefault();
                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.BaseConfig));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.BaseConfig.GUID,
                        ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_contains_Replace_Content_in_Files.ID_RelationType,
                        ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_contains_Replace_Content_in_Files.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                    return result;
                }

                result.Result.Configs = dbReaderSubConfigs.ObjectRels.OrderBy(cfgToCfg => cfgToCfg.OrderID).Select(cfgToCfg => new clsOntologyItem
                {
                    GUID = cfgToCfg.ID_Other,
                    Name = cfgToCfg.Name_Other,
                    GUID_Parent = cfgToCfg.ID_Parent_Other,
                    Type = cfgToCfg.Ontology
                }).ToList() ;
                if (!result.Result.Configs.Any())
                {
                    result.Result.Configs.Add(result.Result.BaseConfig);
                }

                var searchCaseSensitives = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Case_Sensitive.GUID
                }).ToList();


                var dbReaderCaseSensitives = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCaseSensitives.GetDataObjectAtt(searchCaseSensitives);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Case Sensitive Attributes!";
                    return result;
                }

                result.Result.ConfigsCaseSensitive = dbReaderCaseSensitives.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsCaseSensitive));

                var searchFilter = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Filter.GUID
                }).ToList();


                var dbReaderFilter = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFilter.GetDataObjectAtt(searchFilter);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Filter Attributes!";
                    return result;
                }

                result.Result.ConfigsFilter = dbReaderFilter.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsFilter));


                var searchRecursive = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Recursive.GUID
                }).ToList();


                var dbReaderRecursive = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRecursive.GetDataObjectAtt(searchRecursive);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Recursive Attributes!";
                    return result;
                }

                result.Result.ConfigsRecursive = dbReaderRecursive.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsRecursive));


                var searchRegex = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_RegEx.GUID
                }).ToList();


                var dbReaderRegex = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRegex.GetDataObjectAtt(searchRegex);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Regex Attributes!";
                    return result;
                }

                result.Result.ConfigsRegex = dbReaderRegex.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsRegex));


                var searchReplaceString = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Replace_String.GUID
                }).ToList();


                var dbReaderReplaceString = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReplaceString.GetDataObjectAtt(searchReplaceString);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Replace String Attributes!";
                    return result;
                }

                result.Result.ConfigsReplaceString = dbReaderReplaceString.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsReplaceString));


                var searchSearch = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Search.GUID
                }).ToList();


                var dbReaderSearch = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSearch.GetDataObjectAtt(searchSearch);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Search Attributes!";
                    return result;
                }

                result.Result.ConfigsSearch = dbReaderSearch.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsSearch));

                var TestTest = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Test.GUID
                }).ToList();


                var dbReaderTest = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTest.GetDataObjectAtt(TestTest);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Test Attributes!";
                    return result;
                }

                result.Result.ConfigsTest = dbReaderTest.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsTest));


                var searchConfigsToPath = result.Result.Configs.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_search_in_Path.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_search_in_Path.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToPath = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToPath.GetDataObjectRel(searchConfigsToPath);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Paths!";
                    return result;
                }

                result.Result.ConfigsToPath = dbReaderConfigsToPath.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToPath));

                var searchConfigsToExcludePath = result.Result.Configs.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_exclude_Path.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_exclude_Path.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToExcludePath = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToExcludePath.GetDataObjectRel(searchConfigsToExcludePath);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the ExcludePaths!";
                    return result;
                }

                result.Result.ConfigsToExcludePath = dbReaderConfigsToExcludePath.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToExcludePath));

                var searchConfigsToBackupPath = result.Result.Configs.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_Backup_Path.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_Backup_Path.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToBackupPath = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToBackupPath.GetDataObjectRel(searchConfigsToBackupPath);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the BackupPaths!";
                    return result;
                }

                result.Result.ConfigsToBackupPath = dbReaderConfigsToBackupPath.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToBackupPath));

                var searchConfigsBakupRootPaths = result.Result.Configs.Select(cfg => new clsObjectAtt
                {
                    ID_Object = cfg.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_Backup_Rootpath.GUID
                }).ToList();

                var dbReaderConfigsBackupRootPaths = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsBackupRootPaths.GetDataObjectAtt(searchConfigsBakupRootPaths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Paths for Backup-Folder!";
                    return result;
                }

                result.Result.ConfigsBackupRootPath = dbReaderConfigsBackupRootPaths.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsBackupRootPath));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigsToTestPaths = result.Result.Configs.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_Test_Path.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_Test_Path.ID_Class_Right
                }).ToList();

                var dbReaderConfigsTestPaths = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsTestPaths.GetDataObjectRel(searchConfigsToTestPaths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Paths for Test-Folder!";
                    return result;
                }

                result.Result.ConfigsToTestPath = dbReaderConfigsTestPaths.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToTestPath));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigsToReportReplaces = result.Result.Configs.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_contains_Replace_Content_by_Report.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_in_Files_contains_Replace_Content_by_Report.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToReportReplaces = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToReportReplaces.GetDataObjectRel(searchConfigsToReportReplaces);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Replaces!";
                    return result;
                }

                result.Result.ConfigsToReportReplace = dbReaderConfigsToReportReplaces.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToReportReplace));

                var searchReportReplacesToCommandLineRuns = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_search_Comand_Line__Run_.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_search_Comand_Line__Run_.ID_Class_Right
                }).ToList();

                searchReportReplacesToCommandLineRuns.AddRange(dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_replace_with_Comand_Line__Run_.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_replace_with_Comand_Line__Run_.ID_Class_Right
                }));

                var dbReaderReportReplacesToCommandLineRun = new OntologyModDBConnector(globals);
                if (searchReportReplacesToCommandLineRuns.Any())
                {
                    result.ResultState = dbReaderReportReplacesToCommandLineRun.GetDataObjectRel(searchReportReplacesToCommandLineRuns);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the CommandLIneRuns of Report-Replaces!";
                        return result;
                    }
                }
                
                result.Result.ReportReplacesToCommandLineRuns = dbReaderReportReplacesToCommandLineRun.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToCommandLineRuns));

                var searchReportReplacesToFilePathField = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_File_Path_Report_Field.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_File_Path_Report_Field.ID_Class_Right
                }).ToList();

                var dbReaderReportReplacesToFilePathField = new OntologyModDBConnector(globals);

                if (searchReportReplacesToFilePathField.Any())
                {
                    result.ResultState = dbReaderReportReplacesToFilePathField.GetDataObjectRel(searchReportReplacesToFilePathField);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the File-Path Report-Field of Report-Replaces!";
                        return result;
                    }
                }

                result.Result.ReportReplacesToReportFieldFilePaths = dbReaderReportReplacesToFilePathField.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToReportFieldFilePaths));


                var searchReportReplacesToReportFilter = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Report_Filter.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Report_Filter.ID_Class_Right
                }).ToList();

                var dbReaderReportReplacesToReportFilter = new OntologyModDBConnector(globals);

                if (searchReportReplacesToReportFilter.Any())
                {
                    result.ResultState = dbReaderReportReplacesToReportFilter.GetDataObjectRel(searchReportReplacesToReportFilter);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Filters!";
                        return result;
                    }
                }

                result.Result.ReportReplacesToReportFilters = dbReaderReportReplacesToReportFilter.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToReportFilters));



                var searchReportReplacesToReport = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_belonging_Reports.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_belonging_Reports.ID_Class_Right
                }).ToList();

                var dbReaderReportReplacesToReport = new OntologyModDBConnector(globals);

                if (searchReportReplacesToReport.Any())
                {
                    result.ResultState = dbReaderReportReplacesToReport.GetDataObjectRel(searchReportReplacesToReport);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Reports!";
                        return result;
                    }
                }
                

                result.Result.ReportReplacesToReports = dbReaderReportReplacesToReport.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToReports));

                var searchReportReplacesToReportSort = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Report_Sort.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Report_Sort.ID_Class_Right
                }).ToList();

                var dbReaderReportReplacesToReportSort = new OntologyModDBConnector(globals);

                if (searchReportReplacesToReportSort.Any())
                {
                    result.ResultState = dbReaderReportReplacesToReportSort.GetDataObjectRel(searchReportReplacesToReportSort);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Sorts!";
                        return result;
                    }
                }
                    
                result.Result.ReportReplacesToReportSorts = dbReaderReportReplacesToReportSort.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToReportSorts));


                var searchReportReplacesToVariableToField = dbReaderConfigsToReportReplaces.ObjectRels.Select(reportReplace => new clsObjectRel
                {
                    ID_Object = reportReplace.ID_Other,
                    ID_RelationType = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Variable_To_Field.ID_RelationType,
                    ID_Parent_Other = ReplaceContent.Config.LocalData.ClassRel_Replace_Content_by_Report_uses_Variable_To_Field.ID_Class_Right
                }).ToList();

                var dbReaderReportReplacesToVariableToField = new OntologyModDBConnector(globals);

                if (searchReportReplacesToVariableToField.Any())
                {
                    result.ResultState = dbReaderReportReplacesToVariableToField.GetDataObjectRel(searchReportReplacesToVariableToField);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Variable to Report-Fields!";
                        return result;
                    }
                }

                result.Result.ReportReplacesToVariableFields = dbReaderReportReplacesToVariableToField.ObjectRels;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ReportReplacesToVariableFields));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigToNoLogging = result.Result.Configs.Select(config => new clsObjectAtt
                {
                    ID_Object = config.GUID,
                    ID_AttributeType = ReplaceContent.Config.LocalData.AttributeType_No_Logging.GUID
                }).ToList();

                var dbReaderNoLogging = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderNoLogging.GetDataObjectAtt(searchConfigToNoLogging);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the No-Logging-Attribute";
                    return result;
                }

                result.Result.ConfigsToNoLogging = dbReaderNoLogging.ObjAtts;

                result.ResultState = ValidationController.ValidateReplaceContentModel(result.Result, globals, nameof(ReplaceContentModel.ConfigsToNoLogging));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RelateExternalFilesModel>> GetRelateExternalFilesModel(RelateExternalFilesRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<RelateExternalFilesModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new RelateExternalFilesModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetReferenceExternalFilesModel(result.Result, dbReaderConfig, globals, nameof(RelateExternalFilesModel.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchSubItems = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object= result.Result.Config.GUID,
                        ID_AttributeType = RelateExternalFiles.Config.LocalData.AttributeType_Subitems.GUID
                    }
                };

                var dbReaderSubItems = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubItems.GetDataObjectAtt(searchSubItems);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Subitems-Attribute!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetReferenceExternalFilesModel(result.Result, dbReaderSubItems, globals, nameof(RelateExternalFilesModel.SubItems));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigsToFolders = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = RelateExternalFiles.Config.LocalData.ClassRel_Relate_external_Files_compare_Folder.ID_RelationType,
                        ID_Parent_Other = RelateExternalFiles.Config.LocalData.ClassRel_Relate_external_Files_compare_Folder.ID_Class_Right
                    }
                };

                var dbReaderConfigsToFolders = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToFolders.GetDataObjectRel(searchConfigsToFolders);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Folders to compare!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetReferenceExternalFilesModel(result.Result, dbReaderConfigsToFolders, globals, nameof(RelateExternalFilesModel.ConfigToFolders));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchCreateBlobFiles = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_AttributeType = RelateExternalFiles.Config.LocalData.ClassAtt_Relate_external_Files_Create_blob_if_new.ID_AttributeType
                    }
                };

                var dbReaderCreateBlobFiles = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCreateBlobFiles.GetDataObjectAtt(searchCreateBlobFiles);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Atribute ot create blob if new file!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetReferenceExternalFilesModel(result.Result, dbReaderCreateBlobFiles, globals, nameof(RelateExternalFilesModel.CreateBlobIfNewFile));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
